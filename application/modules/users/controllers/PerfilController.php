<?php

/**
 * Clase para menejo de perfil de usuario [publico y privado]
 * @author pablo
 *
 */

class users_PerfilController extends My_Controller_Action
{
	private $_filename;
	
	
	public function indexAction()
	{
		$validador = new My_Validador();
		$usuario   = new My_Model_Usuario();
		
		$request = $this->getRequest();
		if ($this->getRequest()->isPost()) {
			$profile = $request->getPost();
			$images = new My_Model_Images();
			$this->_filename = $images->uploadfile($this->_auth['id']);
			
			$permitidos = array(' ','@','_','-','"','.');
			
			$data = array(
					'idUsario'     => $validador->intValido($this->_auth['id']),
					'fotoperil'    => $validador->alphanumValido($this->_filename,$permitidos),
					'acercademi'   => ($profile['user_profile_description'])?$validador->wysiwyg($profile['user_profile_description'], $permitidos):'',
					'nombreUsuario'=> $validador->alphanumValido($profile['user_profile_fullname'], $permitidos), 
					);
			$usuario->datosPerfil($data);
			$this->_helper->flashMessenger->addMessage('success | Tu perfil se ha actualizado correctamente');
			$this->_redirect('/users/perfil');
		}
		
		$datauser = $usuario->getDatosPerfil($this->_auth['id']);
		
		if ($datauser) {
			$this->view->nombreUsuario  = $datauser->getNombreUsuario();
			$this->view->nombreCompleto = $datauser->getNombreCompleto();
			$this->view->urlfotoperfil  = $datauser->getFotoPerfil();
			$this->view->acercademi     = $datauser->getacercademi();
		}
	}
	
	
	public function cambiocontrasenaAction(){
		$usuario   = new My_Model_Usuario();
		$request = $this->getRequest();
		
		if ($this->getRequest()->isPost()) {
			$profile = $request->getPost();
			
			$password = hash("whirlpool",addslashes(addslashes(trim($profile['user_current_password']))));
			
			if(isset($profile['user_current_password_acutal'])){
				$curren_pass = hash("whirlpool",addslashes(addslashes(trim($profile['user_current_password_acutal']))));
				$valido =  $usuario->comparePass($this->_auth['id'], $curren_pass);
				if($valido) { 
					$result =  $usuario->savepass($this->_auth['id'], $password);
					$this->_helper->flashMessenger->addMessage('success | La contrase&ntilde;a se cambio con &eacute;xito');
					$this->_redirect('/users/perfil/cambiocontrasena/');
				} else {
					$this->_helper->flashMessenger->addMessage('error | La contrase&ntilde;a actual no coincide');
					$this->_redirect('/users/perfil/cambiocontrasena/');
				}
			} else {
				$usuario->savepass($this->_auth['id'], $password);
				$this->_helper->flashMessenger->addMessage('success | La contrase&ntilde;a se asigno con &eacute;xito');
				$this->_redirect('/users/perfil/cambiocontrasena/');
			}
			
		}
		
		$datauser =  $this->_em->find("Default_Model_Usuario",$this->_auth['id']);
		$pass     =  $datauser->getPassword();
		$getFBid  =  $datauser->getfacebookid();
		
		if($pass === null && $getFBid != '' ){
			$this->view->facebookUser = '1';
		}
	}
	
	
	public function publicoAction(){
		$validador = new My_Validador();
		//Zend_Debug::dump($this->getRequest()->getParam('username'));die;
		$username  = $this->getRequest()->getUserParam('username','');
		$usuario   = new My_Model_Usuario();
		
		$objUser   = $usuario->getUserbyUserName($username);
		
		if(!$objUser) {
			$this->_redirect('/');
		}
		
		$this->view->username       = $objUser[0]->getNombreUsuario();
		$this->view->nombrecompleto = $objUser[0]->getNombreCompleto();
		$this->view->acercademi     = $objUser[0]->getacercademi();
		$this->view->fotoperfil     = $objUser[0]->getFotoPerfil();
		
			
		$Subastas = $this->_em->getRepository('Default_Model_ArticuloSubastado')->findBy(array('usuario'=>$objUser[0],'status'=>array('1','2')));
		
		$x=0;
		foreach ($Subastas as $subasta){
			$x++;
			if(strlen($subasta->getTitulo())>64)
				$titulo = substr($subasta->getTitulo(), 0,62).'...';
			else
				$titulo = $subasta->getTitulo();
		
			switch($subasta->getTipoSubastaText()){
				case "inglesa":
					$monto 	=	$subasta->getPujaMayor();break;
				case "vickrey":
					$monto 	=	$subasta->getPrecioInicial();break;
				case "holandesa":
					switch($subasta->getStatusSubastaText()){
						case "por comenzar":
							$monto = $subasta->getPrecioInicial();break;
						case "en progreso":
							$arreglo =  $subasta->getPujaHolandesa();
							$monto	=($arreglo["monto"]*1);break;
						case "finalizada":
							$monto	= 	$subasta->getPrecioFinal();break;
					}
					break;
			}
			$moneda =	 new Zend_Currency(array('value'=>$monto));
			$precioIni =	 new Zend_Currency(array('value'=>$subasta->getPrecioInicial()));
			$urlPub = "/subastas/subasta/show/id/".$subasta->getId()."/".filter_var(str_replace(" ", "-",  $titulo),FILTER_SANITIZE_URL);
			$urlPub = strtolower($urlPub);
		
			$fotos	= $subasta->getFotos();
			$subastasArray[]=array(
					'id'			=>	$subasta->getId(),
					'titulo'		=>	$titulo,
					'nombreUsuario'	=>	substr($subasta->getUsuario()->getNombreUsuario(),0,15),
					'imagen'		=>	$fotos[0]->getFileName(),
					'precio'		=>	($subasta->getStatus()==1)?$moneda->toString():'Finalizada',
					'precioIni'		=>	$precioIni->toString(),
					'urlPub'		=>	$urlPub,
					'status'		=>	$subasta->getStatus()
			);
		}
		$this->view->pubs	=	$subastasArray;
		
	}
} 