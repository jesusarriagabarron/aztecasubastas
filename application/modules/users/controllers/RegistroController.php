<?php

/**
 * Clase para manejo de usuarios ['Alta, edicion, baja']
 * @author pablo
 *
 */

require_once 'facebookSDK/facebook.php';

class users_RegistroController extends My_Controller_Action
{
	private $appid;
	private $secret;
	
	/**
	 * Inicializamos variables de configuración
	 */
	public function init(){
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$fbkeys = $config->getOption('facebook');
		$this->appid  = $fbkeys['appid'];
		$this->secret = $fbkeys['appsecret'];
	}
	
	
	/**
	 * Extrae información del usuario de facebook
	 */
	private function getSessionDataUser(){
		$facebook = new Facebookphp(array(
				'appId'  => $this->appid,
				'secret' => $this->secret,
				'cookie' => true
		));
		
		$idUserFB =  $facebook->getUser();
		if($idUserFB != 0 ) {
			$dataUser = $facebook->api('/'.$facebook->getUser());
			return  $dataUser;
		} else {
			return false;
		}
	}
	
	
	/**
	 * Login mediante facebook
	 */
	
	public function loginfbAction(){
		$usuario  = new My_Model_Usuario();
		$dataUser = $this->getSessionDataUser();
		
		//Validamos si el usuario ya esta registrado
		$recordusuario = $usuario->getuserbyidfb($dataUser['id']);
		if(count($recordusuario) == 0) {
			//Validamos que el usuario proporcione un email, en caso contrario pedimos uno
			if (!isset($dataUser['email']) || $dataUser['email'] == '' ) {
				$form    = new Application_Form_registrofb();
				$this->view->form = $form;
				$this->view->nombrecompleto = $dataUser['name'];
				$this->view->username       = $dataUser['username'];
				$this->view->facebookid     = $dataUser['id']; 
				
				$request = $this->getRequest();
				
				if ($this->getRequest()->isPost()) {
					if ($form->isValid($request->getPost())) {
						$datos  = $form->getValues();
						$result = $usuario->recorduserface($datos);
						if($result) {
							$result = $usuario->userloginfb($datos['email'],$datos['id']);
							$this->_redirect("/");
						} else { }
					}
				}
			} else {
				//Registro de Usuario
				$result = $usuario->recorduserface($dataUser);
				if($result) {
					$result = $usuario->userloginfb($dataUser['email'],$dataUser['id']);
					$this->_redirect("/");
				} else {}
			}
		} else {
			$result =  $usuario->userloginfb($recordusuario[0]['email'],$dataUser['id']);
			$this->_redirect("/");
		}
	}
	
	
	/**
	 * Action principal
	 */
	public function indexAction() {
		$this->_helper->layout->disableLayout();
		
		$request = $this->getRequest();
		$form    = new Application_Form_usuarios();
		
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($request->getPost())) {
				$values = $form->getValues();
				$this->view->save =  $this->recorduser($values);
				$this->view->mail = $values['eamil'];
				$this->view->pass = $values['password'];
			}
		}
		$this->view->form = $form;
	}
	
	/**
	 * Acción de logueo despues de 
	 */
	public function loginbrAction(){
		$this->_helper->layout->disableLayout();
		
		$this->view->logueado = 'false';
		
		$user    = new My_Model_Usuario();
		$request = $this->getRequest();
		
		if ($this->getRequest()->isPost()) {
			$datauser = $request->getPost();
			$login    = $user->userloginflat($datauser['username'],$datauser['password']);
		}
		if($login){
			$this->view->logueado = 'true';
		}
	}
	
	
	/**
	 * Guarda registros
	 * @param array $data
	 */
	private function recorduser($data){
		$usuario = new My_Model_Usuario();
		return $usuario->saveUser($data);
	}
} 