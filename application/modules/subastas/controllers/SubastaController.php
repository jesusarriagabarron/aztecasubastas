<?php
/**
 * Clase Subasta - controlador que maneja las subastas
 * @author rlunam
 *
 */
include_once  APPLICATION_PATH.'/../cron/subastaCheck.php';

class subastas_subastaController extends My_Controller_Action {

	/**
	 * Metodo que muestra los detalles de una subasta especifica
	 */
	public function showAction(){
			$request		=		$this->getRequest();
			$idSubasta 		=		$request->getParam("id",0);
			$Subasta		=	$this->_em->find("Default_Model_ArticuloSubastado",$idSubasta);
			if(!$Subasta){
				$this->_helper->flashMessenger->addMessage('error | La subasta no existe ');
				$this->_redirect('/');
			}
			
			if(!$this->_auth){
				$_SESSION["urlVisita"]=  $this->getRequest()->getRequestUri();
			}
			
			
			
			$ganador 	= 	$Subasta->getGanador($this->_em);
			if($ganador){
				$this->view->Ganador	=	$ganador["ganador"];
				$this->view->PujaGanador=	$ganador["precioFinal"];
			}
			
			$Subasta->setVista();
			$this->_em->persist($Subasta);
			$this->_em->flush();
			
			$instrucciones = 	$Subasta->getInstrucciones();
			$instrucciones =	explode("||",$instrucciones);
			$this->view->instrucciones	=	$instrucciones;
			$this->view->StatusSubasta	=	$Subasta->getStatusSubastaText();
			$this->view->Subasta	=	$Subasta;
	}
	
	/**
	 * Metodo que muestra las pujas de un artículo subastado
	 */
	public function verPujasAction(){
		$request		=		$this->getRequest();
		$idSubasta 		=		$request->getParam("id",0);
		$Subasta		=	$this->_em->find("Default_Model_ArticuloSubastado",$idSubasta);
		if(!$Subasta){
			$this->_helper->flashMessenger->addMessage('error | La subasta no existe ');
			$this->_redirect('/');
		}
		
		if(strlen($Subasta->getTitulo())>64)
			$titulo = substr($Subasta->getTitulo(), 0,62).'...';
		else
			$titulo = $Subasta->getTitulo();
		$urlSub = strtolower("/subastas/subasta/show/id/".$Subasta->getId()."/".filter_var(str_replace(" ", "-",  $titulo),FILTER_SANITIZE_URL));
		if($Subasta->getTipoSubastaText()=="vickrey"){
			$query	=	$this->_em->createQuery("	SELECT 	p
												FROM 	Default_Model_Puja p
												WHERE p.articuloSubastado = ?1
												ORDER BY p.fechaPuja DESC");
			$finalizo=false;
			if($Subasta->getStatusSubastaText()=="finalizada")
				$finalizo=true;
			if($Subasta->getUsuario()->getId()==$this->_auth["id"])
				$finalizo=true;
		}else{
		$query	=	$this->_em->createQuery("	SELECT 	p 
												FROM 	Default_Model_Puja p 
												WHERE p.articuloSubastado = ?1 
												ORDER BY p.precio DESC, p.fechaPuja ASC");
				$finalizo=true;
		}
		$query->setParameter(1,$Subasta);
		$pujas = array();
		foreach($query->getResult() as $puja){
			$pujax = array();
			$pujax["usuario"]	=	$puja->getUsuario()->getNombreUsuario();
			$pujax["precio"]		=	($finalizo==true)?$puja->getPrecio():"<i class=\"fa fa-eye-slash\"></i> secreto ";
			$pujax["fecha"]		=	$puja->getFechaPuja();
			array_push($pujas,$pujax);
		}
		
		$imagen = $Subasta->getFotos();
		$this->view->imagen	=	$imagen[0]->getFileName();
		$this->view->urlSub		=	$urlSub;
		$this->view->Pujas		=	$pujas;
		$this->view->Subasta	=	$Subasta;
		$this->view->Ganador	=	$Subasta->getGanador($this->_em);

	}
	
	/**
	 * Genera eventos de una subasta para los clientes...
	 */
	public function statusAction(){
		Zend_Layout::getMvcInstance()->disableLayout();
		Header ('Content-Type: text/event-stream');
		$request 	=	$this->getRequest();
		$idSubasta	=	$request->getParam("id",0);
		$Subasta	=	$this->_em->find("Default_Model_ArticuloSubastado",$idSubasta);
		$this->view->Subasta=false;
		if($Subasta){
				$this->view->Subasta = $Subasta;			
		}
	}
	
	public function checksubastaAction(){
		$subastas 	=	new subastaCheck();
		$subastas->verificaVencidas();
		$ok='ok';
		$this->_helper->json->sendJson($ok);
	}
	
	public function simulacionAction(){
		$request	=	$this->getRequest();
		$fi			=	$request->getParam("fi")/1000;
		$fc			=	$request->getParam("fc")/1000;
		$max		=	$request->getParam("max");
		$res		=	$request->getParam("res");
		$intervalo	=	$request->getParam("interval");
		$fechaI		=	$fi;
		$fechaF		=	$fc;
		
		$t				=	$fechaF - $fechaI;
		$minutos	  	=	$t/60;
		$numIntervalos 	=	$minutos/$intervalo;
		$dineroDisponible	=	$max - $res;
		$decremento		=	$dineroDisponible/$numIntervalos;	

		$tabla = array();
		for($i=1;$i<=$numIntervalos;$i++){
			$f	=	new DateTime('now'); 
			$fechaI		= 	$fechaI + (($intervalo)*60);
			$max		=	$max - $decremento;
			$money		=	new Zend_Currency();
			$money->setLocale("es_MX");
			$max		=   round($max);
			
			if($max>$res){
				$f->setTimestamp($fechaI);
				$m		=   'MX '.$money->toCurrency($max);
				$tabla[]=array('fecha'=> $f->format('d/m/Y H:i'),'monto'=>$m.'','timestamp'=>$f->getTimestamp());
			}
		}
		$this->_helper->json->sendJson($tabla);
	}
	
	
}