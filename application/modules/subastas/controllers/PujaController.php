<?php
/**
 * Clase Puja - controlador que maneja las pujas que se realicen en la subasta
 * @author rlunam
 *
 */
class subastas_pujaController extends My_Controller_Action {
	// Inicia la confirmacion de la puja
	public function indexAction(){
		$request	=	$this->getRequest();
		$idSubasta 	= (int)	$request->getParam('id',0);
		$tuOferta			=		$request->getParam("precioPuja",0);
		if($request->isPost()){
			$subasta 	=	$this->_em->find("Default_Model_ArticuloSubastado",$idSubasta);
			if(!$subasta){
				$this->_helper->flashMessenger->addMessage('error | La subasta no existe ');
				$this->_redirect('/');
			}
			if(strlen($subasta->getTitulo())>64)
				$titulo = substr($subasta->getTitulo(), 0,62).'...';
			else
				$titulo = $subasta->getTitulo();
			$urlPub = strtolower("/subastas/subasta/show/id/".$subasta->getId()."/".filter_var(str_replace(" ", "-",  $titulo),FILTER_SANITIZE_URL));
			if($subasta->getStatus()>1){
				$this->_helper->flashMessenger->addMessage('error | La subasta ha finalizado');
				$this->_redirect($urlPub);
			}
			$this->view->Subasta = $subasta;
			$this->view->tuOferta	=	$tuOferta;
			$imagen = $subasta->getFotos();
			$this->view->imagen	=	$imagen[0]->getFileName();
		}else{
			$this->_redirect("/");
		}
	}
	//Guarda la OFERTA o PUJA de una subasta
	public function saveAction(){
		$request	=	$this->getRequest();
		$idSubasta 	= (int)	$request->getParam('id',0);
		$tuOferta			=		$request->getParam("precioPuja",0);
		$subasta 	=	$this->_em->find("Default_Model_ArticuloSubastado",$idSubasta);
		if(strlen($subasta->getTitulo())>64)
			$titulo = substr($subasta->getTitulo(), 0,62).'...';
		else
			$titulo = $subasta->getTitulo();
		$urlPub = strtolower("/subastas/subasta/show/id/".$subasta->getId()."/".filter_var(str_replace(" ", "-",  $titulo),FILTER_SANITIZE_URL));
		if($subasta->getStatus()>1){
			$this->_helper->flashMessenger->addMessage('error | La subasta ha finalizado');
			$this->_redirect($urlPub);
		}
		$usuario	=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
		
		if($subasta->getTipoSubastaText()=="holandesa"){
		$subasta->setFechaCierra(new DateTime('now'));
		$subasta->setStatus(2);
		$this->_em->persist($subasta);
		}
		
		$nuevaPuja			=	new Default_Model_Puja();
		$nuevaPuja->setFechaPuja();
		$nuevaPuja->setPrecio($tuOferta);
		$nuevaPuja->setSubasta($subasta);
		$nuevaPuja->setUsuario($usuario);
		$this->_em->persist($nuevaPuja);
		
		$this->_em->flush();
		$this->_helper->flashMessenger->addMessage('success | Tu Oferta se recibió correctamente ');
		$this->_redirect($urlPub);
	}
}
