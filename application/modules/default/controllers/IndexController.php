<?php
/**
 * Clase Subasta - controlador que maneja las subastas
 * @author rlunam
 *
 */

include_once  APPLICATION_PATH.'/../cron/subastaCheck.php';

class IndexController extends My_Controller_Action {


		public function inicio(){
				
		$categorias =	$this->_em->getRepository("Default_Model_Categoria")->findAll();
		
		//cargamos las categorias si no hay en la base
		if(count($categorias)==0){
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Inmuebles');
			$this->_em->persist($cat);
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Automotores');
			$this->_em->persist($cat);
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Tecnología');
			$this->_em->persist($cat);
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Hogar y Tú');
			$this->_em->persist($cat);
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Empleo y Educación');
			$this->_em->persist($cat);
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Tiempo Libre y Deportes');
			$this->_em->persist($cat);
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Música');
			$this->_em->persist($cat);
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Negocios y Servicios');
			$this->_em->persist($cat);
			$cat = new Default_Model_Categoria();
			$cat->setNombreCategoria('Otros');
			$this->_em->persist($cat);
			$this->_em->flush();
		}	
	
		$tiposSubasta	=	$this->_em->getRepository("Default_Model_TipoSubasta")->findAll();
		if(count($tiposSubasta)==0){
			$tipoS	= new Default_Model_TipoSubasta();
			$tipoS->setClase("My_Model_Subasta_Inglesa");
			$tipoS->setMetodo("crear");
			$tipoS->setDescripcion("En esta subasta el precio se va incrementando sucesivamente hasta que queda un único comprador, que es el que se adjudica el bien al precio final. Los compradores pueden presentar cuantas pujas deseen mientras que cumplan con la condición de superar a la puja más alta en vigor.");
			$tipoS->setNombre("Subasta Inglesa");
			$this->_em->persist($tipoS);
			
			$tipoS	= new Default_Model_TipoSubasta();
			$tipoS->setClase("My_Model_Subasta_Vickley");
			$tipoS->setMetodo("crear");
			$tipoS->setDescripcion("Es un tipo de subasta de puja secreta, donde los compradores presentan ofertas sin conocer la oferta de las otras personas en la subasta, y en la que gana el postor m&aacute;s alto, pero el precio que se paga es la segunda oferta m&aacute;s alta.");
			$tipoS->setNombre("Subasta Vickley");
			$this->_em->persist($tipoS);
			
			$tipoS	= new Default_Model_TipoSubasta();
			$tipoS->setClase("My_Model_Subasta_Holandesa");
			$tipoS->setMetodo("crear");
			$tipoS->setDescripcion("Los compradores no pujan de forma directa, sino que el que oferta el artículo es quien ofrece una puja máxima, siendo normalmente un precio muy alto, la cual va disminuyendo hasta que alguien decide aceptarla. En el caso de que nadie acepte, se llega al precio mínimo de reserva, el cual es el mínimo al que el poseedor del artículo piensa ofrecerlo.");
			$tipoS->setNombre("Subasta Holandesa");
			$this->_em->persist($tipoS);
			
			$tipoS	= new Default_Model_TipoSubasta();
			$tipoS->setClase("My_Model_Subasta_Yankee");
			$tipoS->setMetodo("crear");
			$tipoS->setNombre("Subasta Yankee");
			$tipoS->setDescripcion("Es una variación de la subasta inglesa, para múltiples artículos idénticos en la cual los ganadores pagan el precio que pujaron por el artículo");
			$this->_em->persist($tipoS);
			
			$this->_em->flush();
		}
		
	}

	
	
	/**
	 * Muestra los espacios publicitarios con status = 1
	 */
	public function indexAction(){
		$this->view->page      =  $this->getRequest()->getParam("p",1);
		$this->view->categoria =  $this->getRequest()->getParam("c",0);
		$subastas 	=	new subastaCheck();
		$subastas->verificaVencidas();
		$this->inicio();
		
	}

	/**
	 * funcion que sirve para cargar los PUBS en un arreglo
	 * @return multitype:NULL mixed unknown Ambigous <string, mixed>
	 */
	public function generarData($categoria){
		if($categoria)
		$Subastas	=	$this->_em->getRepository('Default_Model_ArticuloSubastado')->findBy(array('status'=>array(1,2),'categoria'=>$categoria),array('fechaCreacion'=>'DESC'));
		else 
			$Subastas	=	$this->_em->getRepository('Default_Model_ArticuloSubastado')->findBy(array('status'=>array(1,2)),array('fechaCreacion'=>'DESC'));
		//recorremos el paginador y creamos el arreglo
		if($Subastas){
		$x=0;
		foreach ($Subastas as $subasta){
			$x++;
			if(strlen($subasta->getTitulo())>64)
				$titulo = substr($subasta->getTitulo(), 0,62).'...';
			else
				$titulo = $subasta->getTitulo();

			switch($subasta->getTipoSubastaText()){
				case "inglesa":
					$monto 	=	$subasta->getPujaMayor();break;
				case "vickrey":
					$monto 	=	$subasta->getPrecioInicial();break;
				case "holandesa":
					switch($subasta->getStatusSubastaText()){
						case "por comenzar":
							$monto = $subasta->getPrecioInicial();break;
						case "en progreso":		
							$arreglo =  $subasta->getPujaHolandesa();
							$monto	=($arreglo["monto"]*1);break;
						case "finalizada":
							$monto	= 	$subasta->getPrecioFinal();break;
					}
					break;
			}
			$moneda =	 new Zend_Currency(array('value'=>$monto));
			$precioIni =	 new Zend_Currency(array('value'=>$subasta->getPrecioInicial()));
			$urlPub = "/subastas/subasta/show/id/".$subasta->getId()."/".filter_var(str_replace(" ", "-",  $titulo),FILTER_SANITIZE_URL);
			$urlPub = strtolower($urlPub);
				
			$fotos	= $subasta->getFotos();
			$subastasArray[]=array(
					'id'			=>	$subasta->getId(),
					'titulo'		=>	$titulo,
					'nombreUsuario'	=>	substr($subasta->getUsuario()->getNombreUsuario(),0,15),
					'imagen'		=>	$fotos[0]->getFileName(),
					'precio'		=>	($subasta->getStatus()==1)?$moneda->toString():'Finalizada',
					'precioIni'		=>	$precioIni->toString(),
					'urlPub'		=>	$urlPub,
					'status'		=>	$subasta->getStatus()
			);
		}
		}else{$subastasArray=array();}
		return $subastasArray;
	}
	
	
	/**
	 * Arroja los pubs en formato JSON para mostrarlos en el INDEX principal
	 */
	public function mostrarpubsAction(){
		//obtenemos el parámetro de la página solicitada
		$page      =  $this->getRequest()->getParam("p",1);
		$categoria =  $this->getRequest()->getParam("c",0);
		$pubsCache = $this->generarData($categoria);
	
		$paginador = Zend_Paginator::factory($pubsCache);
		if($page<=$paginador->count()){
		//ponemos la pagina actual
		$paginador->setCurrentPageNumber($page);
		//obtenemos el numero de items por pagina
		$paginador->setItemCountPerPage(20);
		
			foreach ($paginador as $pag){
				$data[] = $pag;
			}
		}else{
			$data=array();
		}
		
		/*if(count($data)<0)
			$data=array();*/
		//arrojamos los datos por JSON
		$this->getHelper('json')->sendJson(array('pubs' => $data,'paginas'=>$paginador->getPages()));
	}
	
	
	/**	
	 * Verifica el login de acceso y crea la session con Auth Adapter
	 */
    public function ingresoAction(){
    	$request = $this->getRequest();
    	$validador = new  My_Validador();
    	//Checamos que sea una petición post
    	if($request->isPost()){
    		//Asignamos los parametros a variables
    		$email 		= $validador->mailValido($request->getParam('inputEmail'));
    		$password 	= $validador->wysiwyg($request->getParam('inputPassword'));
    		
    		//retornamos un error
    		if($email==false){
    		$this->_helper->flashMessenger->addMessage('error | El nombre de usuario es incorrecto');
    		$this->_redirect('/');}
    		
  			$modeloUsuario = new My_Model_Usuario();
   			$respuesta = $modeloUsuario->validarUsuario($email, $password);
    			if($respuesta){
    				//redireccionamos a la cuenta
    				$url = (isset($_SESSION["urlVisita"]))? $_SESSION["urlVisita"] : "/";
    				unset($_SESSION["urlVisita"]);
    				$this->_redirect($url);
    			}
    			else{
	    			//	redireccionamos al inicio con un mensaje de error
    				$this->_helper->flashMessenger->addMessage('error | El nombre de usuario/contraseña es incorrecto');
    				$this->_redirect('/');
    			}
    			
    	} // fin isPOST
    }
 
    

} 