<?php
/**
 * Clase compras 
 * @author rlunam
 *
 */



class cuenta_ComprasController extends Zend_Controller_Action {
	
	private $_em ;
	private $_auth;
	
	
	public function init(){
		//inicializamos la instancia para obtener las variables de autorizacion
		$auth = Zend_Auth::getInstance();
		if($auth->getStorage()->read())
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		//inicializamos doctrine 2
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
		
	}
	
	public function panelAction(){
		$ODC	=		$this->_em->getRepository("Default_Model_OrdenDeCompra")->findBy(array("comprador"=>$this->_auth["id"]));
		$this->view->odc=$ODC;
		
		foreach($ODC as $o){
			if($o->getVistoComprador()==0){
				$o->setVistoComprador();
				$this->_em->persist($o);
			}
			
			$this->_em->flush();
		}
	}
	
	
	public function procesarPagoAction(){
		$request	=		$this->getRequest();
		$odcId		=		$request->getParam("odc",0);
		
		$ODC		=		$this->_em->getRepository("Default_Model_OrdenDeCompra")->findBy(array("comprador"=>$this->_auth["id"],"id"=>$odcId));
		
		if(!$ODC[0]){
			$this->_helper->flashMessenger->addMessage('error | No existe la compra ');
			$this->_redirect('/cuenta/compras/panel');
		}
		
		if(($ODC[0]->getStatus()<>1)){
			$this->_helper->flashMessenger->addMessage('error | Esta compra ya fue pagada');
			$this->_redirect('/cuenta/compras/panel');
		}
		
		$this->view->odc	=	$ODC[0];
	}
	
	/**
	 * Función que guarda un pago de una boleta
	 */
	public function savepaymentAction(){
		$request		=		$this->getRequest();
		$conekta		=		new My_Model_CobrosConekta();
		$filtro			= 		new Zend_Filter_Alnum();
		$chargeId		=		$filtro->filter($request->getParam('ConektaChargeId'));
		$tipoCobro		=		$filtro->filter($request->getParam('tipoCobro'));
		$filtro			=		new Zend_Filter_Int();
		$odc		=	(int)	$filtro->filter($request->getParam('odc'));
		 
	
		if(!$chargeId){
			$this->_helper->flashMessenger->addMessage('error | No existe el cargo o no fué procesado');
			$this->_redirect("/cuenta/compras/panel");
		}
		//consultamos el cargo en conekta
		$cargo 			=		$conekta->getCargo($chargeId);
		
		 
		if(!$cargo->id){
			$this->_helper->flashMessenger->addMessage('error |El cargo no pudo ser procesado por favor comunícate con nosotros contacto@megaquinielabrasil2014.com ');
			$this->_redirect("/cuenta/compras/panel");
		}
		 
		$usuario		=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
		$odc			=	$this->_em->find("Default_Model_OrdenDeCompra",$odc);
		// si el tipo de pago es con tarjeta de credito
		if($tipoCobro=="TC"){
			if($cargo->status=="paid"){
				try{
					$nuevoCargo		=	new Default_Model_Cargo();
					$nuevoCargo->setAmount($cargo->amount);
					$nuevoCargo->setBrand($cargo->payment_method->brand);
					$nuevoCargo->setChargeId($cargo->id);
					$nuevoCargo->setComision($cargo->fee);
					$nuevoCargo->setCurrency($cargo->currency);
					$nuevoCargo->setDescription($cargo->description);
					$nuevoCargo->setFechaHoraTS($cargo->created_at);
					$nuevoCargo->setLast4($cargo->payment_method->last4);
					$nuevoCargo->setNameCard($cargo->payment_method->name);
					$nuevoCargo->setPaymentMethod($cargo->payment_method->object);
					$nuevoCargo->setReference($cargo->reference_id);
					$nuevoCargo->setStatus($cargo->status);
					$nuevoCargo->setUsuario($usuario);
	
	
					$this->_em->persist($nuevoCargo);
					
					$odc->setChargeId($nuevoCargo);
					$odc->setStatus(2); //status pagado
					$odc->setVistoVendedor(0);
					$this->_em->persist($odc);

					$this->_em->flush();
				  
					$this->_helper->flashMessenger->addMessage('success | Tu pago se ha recibido correctamente, en breve el vendedor será notificado 
							para que proceda a enviarte tu compra. ¡Felicidades!');
					$this->_redirect("/cuenta/compras/panel");
				}catch(Exception $e){
					$this->_helper->flashMessenger->addMessage('error | Tu pago No pudo ser procesado');
					$this->_redirect("/cuenta/compras/panel");
				}
	
			} //fin IF cargo->status
		}
		//si el tipo de cobro es con oxxo
		if($tipoCobro=="OXXO"){
			try{
				$nuevoCargo		=	new Default_Model_Cargo();
				$nuevoCargo->setAmount($cargo->amount);
				//$nuevoCargo->setBrand($cargo->payment_method->brand);
				$nuevoCargo->setChargeId($cargo->id);
				$nuevoCargo->setComision($cargo->fee);
				$nuevoCargo->setCurrency($cargo->currency);
				$nuevoCargo->setDescription($cargo->description);
				$nuevoCargo->setFechaHoraTS($cargo->created_at);
				//$nuevoCargo->setLast4($cargo->payment_method->last4);
				//$nuevoCargo->setNameCard($cargo->payment_method->name);
				$nuevoCargo->setPaymentMethod($cargo->payment_method->type);
				$nuevoCargo->setReference($cargo->reference_id);
				$nuevoCargo->setStatus($cargo->status);
				$nuevoCargo->setBarcode($cargo->payment_method->barcode);
				$nuevoCargo->setBarcodeUrl($cargo->payment_method->barcode_url);
				$nuevoCargo->setExpiresAt($cargo->payment_method->expires_at);
				$nuevoCargo->setUsuario($usuario);
	
				$this->_em->persist($nuevoCargo);
	
				$odc->setChargeId($nuevoCargo);
				$this->_em->persist($odc);
				
				$this->_em->flush();
				
	
				$this->_redirect("/cuenta/compras/oxxo/odc/".$odc->getId());
				 
			}catch(Exception $e){
				$this->_helper->flashMessenger->addMessage('error | Tu pago No pudo ser procesado, reporta el numero de boleta y la quiniela ');
				$this->_redirect("/cuenta/compras/panel");
			}
		}
	
		$this->_redirect("/cuenta/compras/panel");
		 
	}
	
	
	/**
	 * Confirmacion de pago con oxxo
	 */
	public function oxxoAction(){
		 
		$request		=		$this->getRequest();
		$conekta		=		new My_Model_CobrosConekta();
		$filtro			=		new Zend_Filter_Int();
		$odc		=	(int)	$filtro->filter($request->getParam('odc'));
	
		if(!$odc){
			$this->_helper->flashMessenger->addMessage('error | No existe la compra');
			$this->_redirect("/cuenta/compras/panel");
		}
	
		$ODC	=	$this->_em->find("Default_Model_OrdenDeCompra",$odc);
		//si no hay boleta a la goma
		if(!$ODC){
			$this->_helper->flashMessenger->addMessage('error | No existe la compra');
			$this->_redirect("/cuenta/compras/panel");
		}
		//si no le pertenece la boleta al usuario a la goma
		if($ODC->getComprador()->getId()<>$this->_auth["id"] || !$ODC->getChargeId() ){
			$this->_helper->flashMessenger->addMessage('error | No existe la compra');
			$this->_redirect("/cuenta/compras/panel");
		}
		 
		$tipoCargo =  $ODC->getChargeId()->getPaymentMethod();
		//si el cargo no fue OXXO a la goma
		if($tipoCargo<>"oxxo"){
			$this->_helper->flashMessenger->addMessage('error | No existe la compra');
			$this->_redirect("/cuenta/compras/panel");
		}
		//consultamos el cargo en conekta
		$cargo 			=		$conekta->getCargo($ODC->getChargeId()->getChargeId());
		if(!$cargo->id){
			$this->_helper->flashMessenger->addMessage('error | No existe la compra');
			$this->_redirect("/cuenta/compras/panel");
		}

		
		/*
		 if($cargo->status=="paid"){
		$this->_helper->flashMessenger->addMessage('success | La boleta No.'.$idBoleta.' ya fue pagada, y esta participando');
		$this->_redirect("/cuenta/control");
		}*/
		 
		//ENVIAR MAIL DEL CODEBAR
		 
		 
		// YA DESPUES DE VALIDAR ahora si mostramos el codigo de barras en pantalla
		$this->view->barcode 		= $cargo->payment_method->barcode;
		$this->view->barcodeUrl		= $cargo->payment_method->barcode_url;
		$this->view->expira			= $cargo->payment_method->expires_at;
		 
	}
	
}