<?php
/**
 * Clase ventas 
 * @author rlunam
 *
 */
class cuenta_VentasController extends My_Controller_Action {
	/**
	 * panel de ventas realizadas 
	 */
	public function panelAction(){
			$ODC	=		$this->_em->getRepository("Default_Model_OrdenDeCompra")->findBy(array("vendedor"=>$this->_auth["id"]));
			$this->view->odc=$ODC;
			
			foreach($ODC as $o){
				if($o->getVistoVendedor()==0){
					$o->setVistoVendedor();
					$this->_em->persist($o);
				}
					
				$this->_em->flush();
			}
	}
	
	/**
	 * Proceso de envío
	 */
	public function detalleEnvioAction(){
		$request	=		$this->getRequest();
		$odcId		=		$request->getParam("odc",0);
		
		$ODC		=		$this->_em->getRepository("Default_Model_OrdenDeCompra")->findBy(array("vendedor"=>$this->_auth["id"],"id"=>$odcId));
		
		if(!$ODC[0]){
			$this->_helper->flashMessenger->addMessage('error | No existe la venta ');
			$this->_redirect('/cuenta/ventas/panel');
		}
		
		if(($ODC[0]->getStatus()<>2)){
			$this->_helper->flashMessenger->addMessage('error | La venta no ha sido pagada');
			$this->_redirect('/cuenta/ventas/panel');
		}
		
		$cargo	=	new My_Model_CobrosConekta();
		$result = 	$cargo->getCargo($ODC[0]->getChargeId()->getChargeId());
		
		$this->view->odc 	=	$ODC[0];
		$this->view->cargo	=	$result;
		
		
	}
	
	/**
	 * Guardar número guia
	 */
	public function saveGuiaAction(){
		$request	=		$this->getRequest();
		$odcId		=		(int) $request->getParam("odc",0);
		$usuario	=		$this->_em->find("Default_Model_OrdenDeCompra",$this->_auth["id"]);
		$ODC		=		$this->_em->getRepository("Default_Model_OrdenDeCompra")->findBy(array("vendedor"=>$usuario,"id"=>$odcId));
		
		if(!$ODC[0]){
			$this->_helper->flashMessenger->addMessage('error | No existe la venta ');
			$this->_redirect('/cuenta/ventas/panel');
		}
		
		if(($ODC[0]->getStatus()<>2)){
			$this->_helper->flashMessenger->addMessage('error | La venta no ha sido pagada');
			$this->_redirect('/cuenta/ventas/panel');
		}
		
		$ODC[0]->setStatus(3);
		$ODC[0]->setNumeroGuia($request->getParam("numeroguia"));
		$this->_em->persist($ODC[0]);
		$this->_em->flush();
		
		
		$this->_helper->flashMessenger->addMessage('success | Felicidades has realizado tu venta correctamente, en breve realizaremos el depósito 
													de tu pago ');
		$this->_redirect('/cuenta/ventas/panel');
		
	}
		
}