<?php
/**
 * Clase Subasta - controlador que maneja las subastas
 * @author rlunam
 *
 */
class cuenta_SubastaController extends My_Controller_Action {

	/**
	 * Crea una nueva subasta
	 */	
	public function nuevaAction(){
		$request = $this->getRequest();
		//si recibimos el formulario
		if($request->isPost()){
			
			$tablaSubasta 	=	new My_Model_Subasta_ArticuloSubasta();
			$idSubasta =	$tablaSubasta->nuevaSubasta($request);
			$this->_redirect("/cuenta/subasta/configura/id/".$idSubasta);			
			
		}
		$this->view->categorias		=		$this->_em->getRepository("Default_Model_Categoria")->findAll();
	}
	/**
	 * metodo que configura una subasta recién creada o de status "0"
	 */
	public function configuraAction(){
		$request	=	$this->getRequest();
		$idSubasta	=	$request->getParam("id",0);
		//si no hay id de subasta.... a la goma
		if(!$idSubasta){
			$this->_helper->flashMessenger->addMessage('error | No se encontró la subasta');
			$this->_redirect("/");
		}
		//si no hay subasta en la base de datos
		$subasta 	=	$this->_em->find("Default_Model_ArticuloSubastado",$idSubasta);
		if(!$subasta){
			$this->_helper->flashMessenger->addMessage('error | No se encontró la subasta');
			$this->_redirect("/");
		}
		//si la subasta no me pertenece
		if(!($subasta->getUsuario()->getId()==$this->_auth["id"])){
			$this->_helper->flashMessenger->addMessage('error | No se encontró la subasta');
			$this->_redirect("/");
		}
		
		if($request->isPost()){
			$tablaSubasta 	=	new My_Model_Subasta_ArticuloSubasta();
			$idSubasta =	$tablaSubasta->configuraSubasta($request);
			$this->_helper->flashMessenger->addMessage('success | La subasta se ha publicado correctamente');
			$this->_redirect("/");

		}

		$this->view->TipoSubasta = $this->_em->getRepository("Default_Model_TipoSubasta")->findAll();
	}
	
	
	public function misSubastasAction(){
		$this->view->subastas	=	$this->_em->getRepository("Default_Model_ArticuloSubastado")->
		findBy(array('usuario'=>$this->_auth["id"]),array('fechaCreacion'=>'DESC'));	
	}
	
	public function misPujasAction(){
		$usuario	=	$this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
		$dql	=	$this->_em->createQuery("SELECT p,a FROM
						Default_Model_Puja p JOIN p.articuloSubastado a WHERE p.usuario=?1
						ORDER BY a.id DESC, p.precio DESC, p.fechaPuja DESC
						");
		$dql->setParameter(1, $usuario);
		$this->view->em	= $this->_em;
		$this->view->pujas	=	$dql->getResult();
	}
	
	
}