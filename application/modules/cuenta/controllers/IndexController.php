<?php
/**
 * Manejo de una cuenta de un usuario registrado
 * @author jarriaga
 *
 */

class cuenta_IndexController extends Zend_Controller_Action {
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		// TODO Auto-generated indexController::indexAction() default action
		$this->_redirect('/');
	}
	
	/**
	 * Función para el cierre de sesión de una cuenta activa 
	 */
	public function logoutAction(){
		//Instanciamos la Autenticacion
		$auth = Zend_Auth::getInstance();
		//sacamos el almacenamiento de la autenticacion
		$storage = $auth->getStorage();
		//borramos el almacenamiento
		$storage->clear();
		//borramos la identidad de Zend_Auth
		$auth->clearIdentity();
		$this->_redirect('/');
	}
	
	
	public function imageAction(){
		$this->_helper->layout->disableLayout();
		$image = $this->getRequest()->getParam('name',0);
		$size = $this->getRequest()->getParam('size',0);
		if($image){
			$medida="";
			if($size)
				$medida='thumbnail/';
			$imgLocation = APPLICATION_PATH.'/private/images/'.$medida;
			$imgName = $image;
			
			$imgPath = $imgLocation . $imgName;
			
			if(!file_exists($imgPath) || !is_file($imgPath)) {
				header('HTTP/1.0 404 Not Found');
				die('The file does not exist');
			}
			
			$imgData = getimagesize($imgPath);
			if(!$imgData) {
				 
				header('HTTP/1.0 403 Forbidden');
				die('The file you requested is not an image.');
			}
			
			header('Content-type: ' . $imgData['mime']);
			header('Content-length: ' . filesize($imgPath));
			
			readfile($imgPath);
			exit();
		}
	}
	
}
