<?php
/**
 * Archivo de clase para el formulario de login al sistema de facturación
 * inicializa 3 elementos input text usuario, input text password , boton submit
 * @author rlunam
 *
 */
class Application_Form_formlogin extends Zend_Form{
	public function init(){
		$this->setMethod('post');
		$this->setAttrib('style', 'width:300px;text-align:center;margin-left:30px;');
		
		$username= new Zend_Form_Element_Text('username');
		$username->setLabel('')->setAttrib('placeholder','NOMBRE DE USUARIO')
			->setRequired(true)->addErrorMessage('Nombre de usuario es requerido y no puede estar vacío')
			->addFilters(array('StringTrim', 'StripTags'));
		
		$email = new Zend_Form_Element_Text('email');
		$email->setLabel('Email: ')
		->addValidator('EmailAddress');
			
		$passwd = new Zend_Form_Element_Password('password');
		$passwd->setLabel('Contraseña: ')
			->setRequired(true)->addErrorMessage('Escribe tu contraseña de acceso')
			->addFilter('HtmlEntities');
		
		$passwdconf = new Zend_Form_Element_Password('password2');
		$passwdconf->setLabel('Confirma tu contraseña: ')
			->setRequired(true)->addErrorMessage('Confirma tu contraseña')
			->addFilter('HtmlEntities')
			->addValidator('Identical',false,array('token'=>'password'));
		
		$submit = new Zend_Form_Element_Image('submit');
		$submit->setlabel('Submit')
			   ->setAttrib('id', 'saveregistro')
			   ->setAttrib('type', 'submit');
		 	
		$submit = new Zend_Form_Element_Submit('submit');
		$submit->setLabel('Login');
		$submit->setAttrib('class', 'btn btn-block btn-large');
		
		$this->addElement($username);
		$this->addElement($password);
		$this->addElement($submit);
		
	}
}