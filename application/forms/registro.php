<?php

/**
 * Formulario para registro externo
 * @author rlunam
 *
 */

class Application_Form_registro extends Zend_Form{
	public function init(){
		$this->setMethod('post');
		
		// Datos principales de la cuenta
		$nombre = new Zend_Form_Element_Text('nombre');
		$nombre->setLabel('Nombre ó Representante legal: ')
			->setRequired(true)
			->addErrorMessages(array(
					self::isEmpty => "Introduce tu nombre"
			))
			->addFilters(array('StringTrim', 'StripTags'));
			
		
		$apaterno = new Zend_Form_Element_Text('apaterno');
		$apaterno->setLabel('Apellido Paterno: ')
			->setRequired(true)->addErrorMessage('Escribe tu apellido paterno')
			->addFilters(array('StringTrim', 'StripTags'));
		
		$amaterno = new Zend_Form_Element_Text('amaterno');
		$amaterno->setLabel('Apellido Materno: ')
			->addFilters(array('StringTrim', 'StripTags'));
		
		$email = new Zend_Form_Element_Text('email');
		$email->setLabel('Correo electronico: ')
		->addValidator('EmailAddress')
		->addPrefixPath('Validator','Validator/',Zend_Form_Element::VALIDATE)
		->addValidator('UnicUserName');
			
		$passwd = new Zend_Form_Element_Password('password');
		$passwd->setLabel('Contraseña: ')
			->setRequired(true)->addErrorMessage('Escribe tu contraseña de acceso')
			->addFilter('HtmlEntities');
		
		$passwdconf = new Zend_Form_Element_Password('password2');
		$passwdconf->setLabel('Confirma tu contraseña: ')
			->setRequired(true)->addErrorMessage('Confirma tu contraseña')
			->addFilter('HtmlEntities')
			->addValidator('Identical',false,array('token'=>'password'));
		
		$submit = new Zend_Form_Element_Image('submit');
		$submit->setlabel('Submit')
			   ->setAttrib('id', 'saveregistro')
			   ->setAttrib('type', 'submit');
		
		
		$this->addElements(array( 
								  $nombre,
								  $apaterno,
								  $amaterno,
								  $email, 
				                  $passwd,
								  $passwdconf,
								  $submit));
		
	}
}