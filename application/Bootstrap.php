<?php


class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	
	function _initRoutes()
	{   // $this->bootstrap('db');
	}
		
	
	protected function _initAutoload()
	{
		$autoloader = new Zend_Application_Module_Autoloader(array(
				'namespace' => 'Default_',
				'basePath'  => dirname(__FILE__),
		));
		return $autoloader;
	}



	function traduccionErroresFormulario(){
		/** Traduccion de errores **/
			$translate = new Zend_Translate(
				array(
						'adapter' => 'array',
						'content' => array(
								Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => '\'%value%\' ya esta registrado',
								Zend_Validate_NotEmpty::IS_EMPTY=>'El campo es necesario y no puede estar vacío',
								Zend_Validate_EmailAddress::INVALID_FORMAT => '\'%value%\' no es un correo valido'
						),
						'locale' => 'es'
				)
		);
		
		Zend_Validate_NotEmpty::setDefaultTranslator($translate);
		
		/** Fin de traduccion **/
	}

	
	protected function _initView()
		{
			$autoLoader = Zend_Loader_Autoloader::getInstance();
			$autoLoader->registerNamespace('My_');
			Zend_Locale::setDefault('es_MX');
			
			/**
			 * Manejo de caché de datos
			 */
			$frontendOptions = array(
					'lifetime' => 7200,
					'automatic_serialization' => true
			);
			$backendOptions = array(
					'cache_dir' => APPLICATION_PATH.'/../cache'
			);
			$CacheGeneral = Zend_Cache::factory('Core',
					'File',
					$frontendOptions,
					$backendOptions);
			Zend_Registry::set('cache',$CacheGeneral);
			
		    /**
		     * Define la ruta de los helpers views
		     * @var unknown_type
		     */
			$this->traduccionErroresFormulario();
		    $view = new Zend_View();
		    $view->setEncoding('UTF-8');
		    $view->addHelperPath(APPLICATION_PATH.'/../library/My/Helper','My_Helper');
			$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
		    $viewRenderer->setView($view);
		    $fc = Zend_Controller_Front::getInstance();
		    $acl = new My_Acl();
		    $fc->registerPlugin(new My_Plugin_Acl($acl));
		    return $view;	    
		}
	
		
		protected function _initSession()
		{
			Zend_Session::start();
		}
		
		/**
		 * Initialize Doctrine
		 * @return Doctrine_Manager
		 */
		public function _initDoctrine() {
			// include and register Doctrine's class loader
			
			require_once('Doctrine/Common/ClassLoader.php');
			$classLoader = new \Doctrine\Common\ClassLoader(
					'Doctrine',
					APPLICATION_PATH . '/../library/'
			);
			$classLoader->register();
		
			// create the Doctrine configuration
			$config = new \Doctrine\ORM\Configuration();
		
			// setting the cache ( to ArrayCache. Take a look at
			// the Doctrine manual for different options ! )
			$cache = new \Doctrine\Common\Cache\ArrayCache;
			$config->setMetadataCacheImpl($cache);
			$config->setQueryCacheImpl($cache);
		
			// choosing the driver for our database schema
			// we'll use annotations
			$driver = $config->newDefaultAnnotationDriver(
					APPLICATION_PATH . '/models'
			);
			$config->setMetadataDriverImpl($driver);
		
			// set the proxy dir and set some options
			$config->setProxyDir(APPLICATION_PATH . '/models/Proxies');
			$config->setAutoGenerateProxyClasses(true);
			$config->setProxyNamespace('App\Proxies');
		
			// now create the entity manager and use the connection
			// settings we defined in our application.ini
			$connectionSettings = $this->getOption('doctrine');
			$conn = array(
			'driver'    => $connectionSettings['conn']['driv'],
			'user'      => $connectionSettings['conn']['user'],
			'password'  => $connectionSettings['conn']['pass'],
			'dbname'    => $connectionSettings['conn']['dbname'],
			'host'      => $connectionSettings['conn']['host']
				);
			$entityManager = \Doctrine\ORM\EntityManager::create($conn, $config);
		
				// push the entity manager into our registry for later use
			$registry = Zend_Registry::getInstance();
			$registry->entitymanager = $entityManager;
		
			return $entityManager;
		}
}

