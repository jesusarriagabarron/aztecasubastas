<?php


/** @Entity 
 * @Table(name="Cargo")
 * */
class Default_Model_Cargo {
	/**
	 * @Id
	 * @Column(type="string",length=100)
	 */
	private	 	$chargeId;
	/**
	 * @ManyToOne(targetEntity="Default_Model_Usuario",inversedBy="cargos")
	 * @JoinColumn(name="idUsuario",referencedColumnName="id")
	 */
	private 	$usuario;
	
	 /** @Column(type="string",length=50) **/
	private		$status;
	
	/** @Column(type="string",length=4) **/
	private		$currency;
	
	/** @Column(type="string",length=100) **/
	private 	$description;
	
	/** @Column(type="string",length=40) **/
	private 	$reference_id;
	
	/** @Column(type="string",length=50) **/
	private 	$payment_method;
	
	/** @Column(type="string",length=100,nullable=true) **/
	private 	$nameCard;
	
	/** @Column(type="string",length=100,nullable=true) **/
	private 	$last4;
	
	/** @Column(type="string",length=50,nullable=true) **/
	private 	$brand;
	
	
	/** @Column(type="string",length=100,nullable=true) **/
	private 	$barcode;
	
	/** @Column(type="string",length=250,nullable=true) **/
	private 	$barcode_url;
	
	/** @Column(type="integer",nullable=true) **/
	private 	$expires_at;
	
	
	/** @Column(type="integer") **/
	private 	$amount;
	
	/** @Column(type="integer") **/
	private 	$comision;
	
	/** @Column(type="integer") **/
	private 	$fechahora;
	
	
	public function setUsuario(Default_Model_Usuario $usuario){
		if($this->usuario !== $usuario ){
			$this->usuario=$usuario;
			$usuario->setCargo($this);
		}
	}
	/** SETS **/
	public function setChargeId		($chargeId)		{	$this->chargeId		=		$chargeId; 			}
	public function setBarcode		($barcode)		{	$this->barcode		=		$barcode;			}
	public function setBarcodeUrl	($barcodeUrl)	{	$this->barcode_url	=		$barcodeUrl;		}
	public function setExpiresAt	($expiresAt)	{	$this->expires_at	=		$expiresAt;			}
	public function setStatus		($status)		{	$this->status		=		$status;			}
	public function setCurrency		($currency)		{	$this->currency		=		$currency;			}
	public function setDescription	($description)	{	$this->description	=		$description;		}
	public function setReference	($reference_id)	{	$this->reference_id	=		$reference_id;		}
	public function setPaymentMethod($payment)		{	$this->payment_method=		$payment;			}
	public function setNameCard		($nameCard)		{	$this->nameCard		=		$nameCard;				}
	public function setLast4		($last4)		{	$this->last4		=		$last4;				}
	public function setBrand		($brand)		{	$this->brand		=		$brand;				}
	public function setAmount		($amount)		{	$this->amount		=		$amount;			}
	public function setComision		($comision)		{	$this->comision		=		$comision;			}
	public function setFechaHoraTS	($timestamp)	{	$this->fechahora 	= 		$timestamp;			}
	/** GETS **/
	public function getChargeId		()	{	return $this->chargeId;			}
	public function getBarcode		()	{	return $this->barcode;			}
	public function getBarcodeUrl	()	{	return $this->barcode_url;		}
	public function getExpiresAt	()	{	return $this->expires_at;		}
	public function getStatus		()	{	return $this->status;			}
	public function getCurrency		()	{	return $this->currency;			}
	public function getDescription	()	{	return $this->description;		}
	public function getReference	()	{	return $this->reference_id;		}
	public function getPaymentMethod()	{	return $this->payment_method;	}
	public function getNameCard		()	{	return $this->nameCard;			}
	public function getLast4		()	{	return $this->last4;			}
	public function getBrand		()	{	return $this->brand;			}
	public function getAmount		()	{	return $this->amount;			}
	public function getComision		()	{	return $this->comision;			}
	public function getFechaHoraTS	()	{	return $this->fechahora;		}		

	
}