<?php
/** @Entity
 * @Table(name="PujasOfertadas")
 * 
*/
class Default_Model_Puja{
	private $em;
	/**
	 * @Id
	 * @GeneratedValue(strategy="AUTO")
	 * @Column(type="integer")
	 */
	private $id;
	
	/** @Column(type="decimal") **/
	private $precio=0;
	
	/** @Column(type="datetime",nullable=true)  **/
	private $fechaPuja;
	
	/**
	 * @ManyToOne(targetEntity="Default_Model_ArticuloSubastado",inversedBy="pujas")
	 * @JoinColumn(name="idSubasta",referencedColumnName="id")
	 */
	private $articuloSubastado;
	
	
	/**
	 * @ManyToOne(targetEntity="Default_Model_Usuario",inversedBy="pujas")
	 * @JoinColumn(name="idUsuario",referencedColumnName="id")
	 */
	private $usuario;
	
	public function setPrecio($precio){			$this->precio	=	$precio;					}
	public function setFechaPuja(){				$this->fechaPuja=	new DateTime('now');		}
	public function setSubasta(Default_Model_ArticuloSubastado $subasta){	$this->articuloSubastado=$subasta;	}
	public function setUsuario(Default_Model_Usuario $usuario){	$this->usuario	=	$usuario;	}
	
	public function getId(){		return $this->id; 		}
	public function getPrecio(){	return $this->precio;	}
	public function getFechaPuja(){	return $this->fechaPuja;	}
	public function getSubasta(){	return $this->articuloSubastado;}
	public function getUsuario(){				return $this->usuario;			}
	public function setEm($em){
		$this->em=$em;
	}
	
	public function getStatus($idUsuario){
		switch($this->articuloSubastado->getStatusSubastaText()){
			case "en progreso":
				return '<span class="label label-warning">En progreso</span>';break;
			case "finalizada":
				$PujaGanadora = $this->articuloSubastado->getGanador($this->em);
				if($PujaGanadora && $PujaGanadora["ganador"]->getUsuario()->getId()==$idUsuario && $PujaGanadora["ganador"]->getId()==$this->id){
					return '<span class="label label-success">Felicidades ganaste la subasta</span>';
				}else{
					return '<span class="label label-important">Finalizada - no fuiste el ganador</span>';
				}break;
		}
	}
	
}
