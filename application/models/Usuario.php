<?php


/** @Entity 
 * @Table(name="Usuario")
 * */
class Default_Model_Usuario {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;

    /** @Column(type="string", nullable=true,length=150) **/
    private $nombreCompleto;
    
    /** @Column(type="string",length=100) **/
    private $nombreUsuario;
    
    /** @Column(type="string",nullable=true,length=100) **/
	private $fotoPerfil;

	/** @Column(type="string",length=100) **/
	private $urlPerfil;
	
	/** @Column(type="string",unique=true,length=100) **/
	private $email;
	
	/** @Column(type="string", length=300,nullable=true) **/
	private $acercademi;
	
	/** @Column(type="bigint",nullable=true) **/
	private $facebookId;
	
	/** @Column(type="string",nullable=true) **/
	private $twitterId;
	
	/** @Column(type="string",nullable=true,length=200) **/
	private $password;
	
	/** @Column(type="integer") **/
	private $rol;	

	/**
	 * @OneToMany(targetEntity="Default_Model_Cargo",mappedBy="usuario")
	 **/
	private $cargos;

	/**
	 * @OneToMany(targetEntity="Default_Model_ArticuloSubastado",mappedBy="usuario")
	 **/
	private $articulosSubastados;
	
	/**
	 * @OneToMany(targetEntity="Default_Model_OrdenDeCompra",mappedBy="comprador")
	 **/
	private $compras;
	
	/**
	 * @OneToMany(targetEntity="Default_Model_OrdenDeCompra",mappedBy="vendedor")
	 **/
	private $ventas;
	
	
	
	/**
	 * @OneToMany(targetEntity="Default_Model_Puja",mappedBy="usuario")
	 **/
	private $pujas;
	
		
	public function setCargo(Default_Model_Cargo $Cargo){
		$this->cargos[]=$Cargo;
		$Cargo->setUsuario($this);
	}
	
	public function setArticulosSubastados(Default_Model_ArticuloSubastado $articulo){
		$this->articulosSubastados[]=$articulo;
	}
	public function setPuja(Default_Model_Puja $puja){
		$this->pujas[]=$puja;
	}
	
	public function setNombreCompleto($nombre)			{		$this->nombreCompleto=$nombre;			}
	public function setNombreUsuario($nombreUsuario)	{		$this->nombreUsuario=$nombreUsuario;	}
	public function setEmail($email)					{		$this->email = $email;					}
	public function setacercademi($acercademi)			{		$this->acercademi = $acercademi;		}
	public function setRol($rol)						{		$this->rol	=	$rol;					}
	public function setFotoPerfil($fotografia)			{		$this->fotoPerfil = $fotografia;		}
	public function setUrlPerfil($url)					{		$this->urlPerfil = $url;				}
	public function setFacebookId($fbid)				{		$this->facebookId=$fbid;				}
	public function setTwitterId($twitter)				{		$this->twitterid = $twitter;			}
	public function setPassword($password)				{		$this->password = $password;			}
	public function getacercademi () 					{		return $this->acercademi;				}
	public function getNombreUsuario()					{		return $this->nombreUsuario;			}
	public function getRol()							{		return $this->rol;						}
	public function getId()								{		return $this->id;						}
	public function getNombreCompleto()					{		return $this->nombreCompleto;			}
	public function getFotoPerfil () 					{		return $this->fotoPerfil;				}
	public function getEmail()							{		return $this->email;					}
	public function getUrlPerfil()						{		return $this->urlPerfil;				}	
	public function getfacebookid() 					{		return $this->facebookId;				}
	public function getPassword() 						{		return $this->password;					}
	public function getCargos()							{		return $this->cargos;					}
	public function getArticulosSubastados()			{		return $this->articulosSubastados;		}
	public function getPujasUsuario()						{		return $this->pujas;			 	}
}
