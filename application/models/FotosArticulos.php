<?php
/** @Entity */
//require "Doctrine/Common/Collections/ArrayCollection.php";
/** @Entity
 * @Table(name="FotosArticulos")
 * 
*/
class Default_Model_FotosArticulos{

	
	/**
	 * @Id
	 * @GeneratedValue(strategy="AUTO")
	 * @Column(type="integer")
	 */
	private $id;
	
	
	/** @Column(type="string",length=200) **/
	private $filename;
	
	/**
	 * @ManyToOne(targetEntity="Default_Model_ArticuloSubastado",inversedBy="fotos")
	 * @JoinColumn(name="idArticuloSubastado",referencedColumnName="id")
	 */
	private $articuloSubastado;
	
	
	public function setFileName($nombre){			$this->filename		=	$nombre;	}
	public function setArticuloSubastado(Default_Model_ArticuloSubastado $articulo){		$this->articuloSubastado	=	$articulo;	}
	
	public function getFileName()		{		return $this->filename;			}
	public function getArticuloSubastado(){		return $this->articuloSubastado ; }
	public function getId()				{		return $this->id;	}
	
}
