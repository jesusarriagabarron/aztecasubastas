<?php
/** @Entity
 * @Table(name="TipoSubasta")
 * 
*/
class Default_Model_TipoSubasta{

    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    
    
    /** @Column(type="string",length=200) **/
    private $nombre;
    
    /** @Column(type="string",length=100) **/
    private $clase;
    
    /** @Column(type="string",length=100) **/
    private $metodo;
    
    /** @Column(type="text",nullable=true)**/
    private $descripcion;

    /**
     * @OneToMany(targetEntity="Default_Model_ArticuloSubastado",mappedBy="tipoSubasta")
     **/
    private $articulosSubastados;
    
    public function setNombre($nombre){			$this->nombre		=	$nombre;}
    public function setClase($clase){			$this->clase		=	$clase;	}
    public function setMetodo($metodo){			$this->metodo		=	$metodo;}
    public function setDescripcion($descripcion){$this->descripcion	=	$descripcion;	}
    
    
    public function getNombre()		{		return $this->nombre;		}
    public function getId()			{		return $this->id;			}
    public function getClase()		{		return $this->clase;		}
    public function getMetodo()		{		return $this->metodo;		}
    public function getDescripcion(){		return $this->descripcion;	}
}