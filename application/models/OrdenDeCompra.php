<?php
/** @Entity
 * @Table(name="OrdenDeCompra")
 * 
*/
class Default_Model_OrdenDeCompra{

    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    
    /** RELACION Muchos a uno unidireccional
     * @ManyToOne(targetEntity="Default_Model_Usuario",inversedBy="compras")
	 **/
    private $comprador;
    
    /** RELACION Muchos a uno unidireccional
     * @ManyToOne(targetEntity="Default_Model_Usuario",inversedBy="ventas")
     **/
    private $vendedor;
  
    
    /** @Column(type="decimal") **/
    private $precioFinal;
    
    /** @Column(type="integer") **/
    private $status=1;
    
    /** @Column(type="datetime", nullable=true) **/
    private $fechaenvio;
    
    /** @Column(type="datetime") **/
    private $fecha;
    
    /** @Column(type="integer") **/
    private $vistoComprador=0;
    
    /** @Column(type="integer") **/
    private $vistoVendedor=0;
    
    
    /** @Column(type="string",nullable=true) **/
    private $numeroGuia="";
    
    
    /**
     * @OneToOne(targetEntity="Default_Model_Cargo")
     * @JoinColumn(name="chargeId", referencedColumnName="chargeId")
     */
    private $chargeId;
    
    
    /**
     * @OneToOne(targetEntity="Default_Model_ArticuloSubastado", inversedBy="ordenDeCompra")
     * @JoinColumn(name="subastaId", referencedColumnName="id")
     **/
    private $subasta;
  
    
    public function setNumeroGuia($numeroguia){
    	$this->numeroGuia	=	$numeroguia;
    }
    
    public function setChargeId($chargeId){
    	$this->chargeId	=$chargeId;
    }
    
    public function setFechaEnvio($fecha) {
    	$this->fechaenvio = new DateTime($fecha);
    }
    
    public function setFecha(){
    	$this->fecha	=	new DateTime("now");
    }
    
    public function setComprador(Default_Model_Usuario $Usuario){
    	$this->comprador = $Usuario;
    }
    
    public function setVendedor(Default_Model_Usuario $Vendedor){
    	$this->vendedor = $Vendedor;
    }
    
    public function setPrecioFinal($precio){
    	$this->precioFinal = $precio;
    }
    
    public function setStatus($status=1){
    	$this->status = $status;
    }
    
    public function setSubasta($subasta){
    	$this->subasta = $subasta;
    }
    
    public function setVistoComprador($v=1){
    	$this->vistoComprador=$v;
    }
    
    public function setVistoVendedor($v=1){
    	$this->vistoVendedor=$v;
    }
  
    
    /**
     *       GETS
     */
	 
    public function getVistoComprador(){
    	return $this->vistoComprador;
    }
    
    public function getVistoVendedor(){
    	return $this->vistoVendedor;
    }
    
    public function getChargeId(){
    	return $this->chargeId;
    }
    
   	public function getFechaEnvio() {
   		return $this->fechaenvio;
   	} 
   
   	public function	getFecha(){
   		return $this->fecha;
   	}
    
    public function getComprador(){
    	return $this->comprador;
    }
    
    public function getVendedor(){
    	return $this->vendedor;
    }
    
    public function getPrecioFinal(){
    	return $this->precioFinal;
    }
      
    public function getStatus(){
    	return $this->status;
    }
    
    public function getId(){
    	return $this->id;
    }
    
    public function getSubasta(){
    	return $this->subasta;
    }
    
    public function getNumeroGuia(){
    	return $this->numeroGuia;
    }
    
    public function getStatusRow(){
    	$status = "";
    	switch ($this->status){
    		case 1:
    			$status		="warning";
    			break;
    		case 2:
    			$status		="success";
    			break;
    		case 3:
    			$status		="info";
    			break;
    	}
    	return $status;
    }
    
    public function getStatusRowComprador(){
    	$status = "";
    	switch ($this->status){
    		case 1:
    			$status		="warning";
    			break;
    		case 2:
    			$status		="success";
    			break;
    		case 3:
    			$status		="info";
    			break;
    	}
    	return $status;
    }
    
    
    public function getStatusTextoComprador(){
    	$status="";
    	switch ($this->status){
    		case 1:
    			$status='<a href="/cuenta/compras/procesar-pago/odc/'.$this->id.'" class="btn btn-success">Paga aquí tu compra</a>';
    			break;
    		case 2:
    			$status='Tu pago ha sido recibido exitósamente,<br> en breve confirmaremos al vendedor';
    			break;
    		case 3:
    			$status="El comprador te ha enviado tu artículo,<br> tu número guía es el: ".$this->numeroGuia;
    			break;
    	}
    	
    	return $status;
    }
 
    public function getStatusTexto(){
    	$status = "";
    	switch ($this->status){
    		case 1:
    			$status		= "En espera de pago";
    			break;
    		case 2:
    			$status		= 'Hemos recibido el pago de '.$this->comprador->getNombreUsuario().' ,<br> apresúrate a enviar el artículo: <a href="/cuenta/ventas/detalle-envio/odc/'.$this->id.'" class="btn btn-success">Iniciar envío</a>';
    			break;
    		case 3:
    			$status		= 'La venta ha finalizado';
    			break;
    	}
    	return $status;
    }
 
}