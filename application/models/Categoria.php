<?php
/** @Entity
 * @Table(name="Categoria")
 * 
*/
class Default_Model_Categoria{

    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
    
    /** @Column(type="string",length=100) **/
    private $nombreCategoria;
    

    public function setNombreCategoria($nombreCategoria){
    	$this->nombreCategoria = $nombreCategoria;
    }    
    
    public function getNombreCategoria(){
    	return $this->nombreCategoria;
    }
    
    public function getId(){
    	return $this->id;
    }
}