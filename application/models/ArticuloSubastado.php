<?php
/** @Entity
 * @Table(name="Articulosubastado")
 * 
*/
class Default_Model_ArticuloSubastado{
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;
   /** @Column(type="string",length=200) **/
    private $titulo;
	/** @Column(type="text")**/
    private $descripcion;
    /** @Column(type="text")**/
    private $instrucciones;
    /** @Column(type="integer")**/
    private $vistas=0;
    /** @Column(type="datetime",nullable=true)  **/
    private $fechaCreacion;
    /** @Column(type="datetime",nullable=true)  **/
    private $fechaInicio;
    /** @Column(type="datetime",nullable=true)  **/
    private $fechaCierre;
    /** @Column(type="decimal") **/
    private $precioInicial=0;
    /** @Column(type="decimal") **/
    private $precioReserva=0;
    /** @Column(type="decimal") **/
    private $precioFinal=0;
    /** @Column(type="integer") **/
    private $status=0;
    /** @Column(type="integer",options={"default":1}) **/
    private $numeroItems=1;
    /** @Column(type="integer",options={"default":10}) **/
    private $intervalo=10;
    /** RELACION Muchos a uno unidireccional
     * @ManyToOne(targetEntity="Default_Model_Categoria")
     **/
    private $categoria;
    /** RELACION Muchos a uno unidireccional
     * @ManyToOne(targetEntity="Default_Model_TipoSubasta",inversedBy="articulosSubastados")
     * @JoinColumn(name="idTiposubasta",referencedColumnName="id")
     **/
    private $tipoSubasta;
    /**
     * @ManyToOne(targetEntity="Default_Model_Usuario",inversedBy="articulosSubastados")
     * @JoinColumn(name="idUsuario",referencedColumnName="id")
     */
    private $usuario;
    /**
     * @OneToMany(targetEntity="Default_Model_FotosArticulos",mappedBy="articuloSubastado")
     **/
    private $fotos;
    /**
     * @OneToMany(targetEntity="Default_Model_Puja",mappedBy="articuloSubastado")
     **/
    private $pujas;
    
    
    /**
     * @OneToOne(targetEntity="Default_Model_OrdenDeCompra")
     * @JoinColumn(name="odcId", referencedColumnName="id")
     */
    private $ordenDeCompra;
    
    public function setTitulo($titulo){					$this->titulo			=	$titulo;		}
    public function setDescripcion($descripcion){		$this->descripcion		=	$descripcion;	}
    public function setInstrucciones($instrucciones){	$this->instrucciones	=	$instrucciones;	}
    public function setFechaCreacion(){					$this->fechaCreacion	=	new DateTime('now');}
    public function setFechaInicio($fecha){				$this->fechaInicio		= 	$fecha;			}
    public function setFechaCierra($fecha){				$this->fechaCierre		=	$fecha;			}
    public function setPrecioInicial($precioInicial){	$this->precioInicial	=	$precioInicial;	}
    public function setPrecioReserva($reserva){			$this->precioReserva	=	$reserva;		}
    public function setPrecioFinal($preciofinal){		$this->precioFinal		=	$preciofinal;	}
    public function setStatus($status){					$this->status			=	$status;		}
    public function setCategoria(Default_Model_Categoria $categoria){		$this->categoria		=	$categoria;		}
    public function setUsuario(Default_Model_Usuario $usuario){				$this->usuario			=	$usuario;		}
    public function setFoto(Default_Model_FotosArticulos $foto)  {			$this->fotos[]			=	$foto;			}
    public function setTipoSubasta($tipoSubasta){		$this->tipoSubasta	=	$tipoSubasta;	}
    public function setPuja(Default_Model_Puja $puja){	$this->pujas[]=$puja;				}
    public function setVista(){				$this->vistas++;    }
    public function setItems($numeroitems){				$this->numeroItems	=	$numeroitems; 	}
    public function setIntervalo($intervalo){			$this->intervalo	= 	$intervalo;		}
    
    public function getTitulo(){					return $this->titulo;		}
    public function getId(){						return $this->id;			}
    public function getDescripcion(){				return $this->descripcion;	}
    public function getInstrucciones(){				return $this->instrucciones;}
    public function getFechaCreacion(){				return $this->fechaCreacion;}
    public function getFechaInicio(){				return $this->fechaInicio;	}
    public function getFechaCierra(){				return $this->fechaCierre;	}
    public function getPrecioInicial(){				return $this->precioInicial;}
    public function getPrecioReserva(){				return $this->precioReserva;}
    public function getPrecioFinal(){				return $this->precioFinal;	}
    public function getStatus(){					return $this->status;		}
    public function getCategoria(){					return $this->categoria;	}
    public function getUsuario(){					return $this->usuario;		}
    public function getFotos(){						return $this->fotos;		}
    public function getTipoSubasta(){				return $this->tipoSubasta;	}
  	public function getPujas(){						return $this->pujas;		}
  	public function getItems(){						return $this->numeroItems;	}
  	public function getIntervalo(){					return $this->intervalo;	}
  	public function getOrdenDeCompra(){				return $this->ordenDeCompra;}
  	public function getTipoSubastaText(){
  		 switch ($this->tipoSubasta->getId()){
  		 	case 1 : $nombre="inglesa";break;
  		 	case 2 : $nombre="vickrey";break;
  		 	case 3 : $nombre="holandesa";break;
  		 	case 4 : $nombre="yankee";break;
  		 }
  		 return $nombre;
  	}
  	public function getStatusSubastaText(){
  		$actual = new DateTime("now");
  		if($this->status==0){
  			return "sin configurar";
  		}
  		if($this->fechaInicio > $actual && $this->status==1){
  			return "por comenzar";
  		}
  		if($this->fechaInicio <= $actual && $this->status==1){
  			return "en progreso";
  		}
  		if($this->fechaInicio <= $actual && $this->status==2){
  			return "finalizada";
  		}
  	}
  	
  	
  	public function getPujaHolandesa(){
  		
  		switch($this->getStatusSubastaText()){
  			case "por comenzar":
  				$tabla=array('fecha'=> '','monto'=>$this->precioInicial.'','timestamp'=>'');break;
  			case "finalizada":
  					$tabla=array('fecha'=> '','monto'=>$this->precioFinal.'','timestamp'=>'');break;
  			case "en progreso":
  				$max		=	$this->precioInicial;
  				$res		=	$this->precioReserva;
  				$intervalo	=	$this->intervalo;
  				
  				$fechaI		=	$this->fechaInicio->getTimestamp();
  				$fechaF		=	$this->fechaCierre->getTimestamp();
  				
  				$t				=	$fechaF - $fechaI;
  				$minutos	  	=	$t/60;
  				$numIntervalos 	=	$minutos/$intervalo;
  				$dineroDisponible	=	$max - $res;
  				$decremento		=	$dineroDisponible/$numIntervalos;
  				
  				$tabla = array();
  				for($i=1;$i<=$numIntervalos;$i++){
  					$f	=	new DateTime('now');
  					$actualTS = $f->getTimestamp();
  					$fechaI		= 	$fechaI + (($intervalo)*60);
  					$max		=	$max - $decremento;
  					$max		=   round($max);
  					$f->setTimestamp($fechaI);
  					if($max>$res && $actualTS > $f->getTimestamp() ){
  						$m		=   $max;
  						$tabla=array('fecha'=> $f->format('d/m/Y H:i'),'monto'=>$m.'','timestamp'=>$f->getTimestamp());
  					}else{
  						break;
  					}
  				};
  				break;
  					
  		} 
  		
  		
  		return $tabla;
  	}
  	
  	public function getPujaMayor(){
  			$pujas			=	array();
  			$precioMasAlto	= $this->precioInicial;
  			if(count($this->pujas)>0){
  			foreach($this->pujas as $puja){
  					$pujas[]=$puja->getPrecio();	
  			}
  			rsort($pujas);

  			
			if($this->getTipoSubastaText()=="vickrey")
					if(count($this->pujas)>1)
						{$precioMasAlto=$pujas[1];}
					else 	
						{$precioMasAlto=$pujas[0];}
			else 	
					$precioMasAlto=$pujas[0];
  			}
  			
  			return $precioMasAlto;
  	}
  	
  	public function getGanador($em){
  		$data=array();
  		if($this->getStatusSubastaText()=="finalizada"){
  		switch($this->getTipoSubastaText()){
  			case "inglesa":
  				$query	=	$em->createQuery("	SELECT 	p
													FROM 	Default_Model_Puja p
													WHERE p.articuloSubastado = ?1
  													AND	  p.precio >= ?2
													ORDER BY p.precio DESC, p.fechaPuja ASC
												");
  				$query->setMaxResults(1);
  				$query->setParameter(1,$this);
  				$query->setParameter(2,$this->precioReserva);
  				$ganador				=	$query->getResult();
				if($ganador && ($ganador[0]->getPrecio()>=$this->precioReserva)){
					$this->precioFinal	=	$ganador[0]->getPrecio();

					$em->persist($this);
					$em->flush();
					
					//creamos la orden de compra entre ganador y subastador
					//
					if(!$this->ordenDeCompra){
						$ordenDeCompra		=		new Default_Model_OrdenDeCompra();
						$ordenDeCompra->setComprador($ganador[0]->getUsuario());
						$ordenDeCompra->setVendedor($this->usuario);
						$ordenDeCompra->setFecha(new DateTime('now'));
						$ordenDeCompra->setPrecioFinal($ganador[0]->getPrecio());
						$ordenDeCompra->setStatus(1);
						$ordenDeCompra->setSubasta($this);
						$ordenDeCompra->setVistoComprador(0);
						$ordenDeCompra->setVistoComprador(0);
						$this->ordenDeCompra	= $ordenDeCompra;
						$em->persist($ordenDeCompra);
						$em->persist($this);
						$em->flush();
					}
					
					$data=array("ganador"=>$ganador[0],"precioFinal"=>$ganador[0]->getPrecio());
				}  				
  				break;
  			case "vickrey":
  				$query	=	$em->createQuery("	SELECT 	p
													FROM 	Default_Model_Puja p
													WHERE p.articuloSubastado = ?1
													ORDER BY p.precio DESC, p.fechaPuja ASC
												");
  				$query->setMaxResults(2);
  				$query->setParameter(1,$this);
  				$ganador				=	$query->getResult();
  				if($ganador){
  					if(count($ganador)>1){
  						if(($ganador[1]->getPrecio()>=$this->precioReserva)){
  							$this->precioFinal	=	$ganador[1]->getPrecio();
  							$em->persist($this);
  							$em->flush();
  							$data=array("ganador"=>$ganador[0],"precioFinal"=>$ganador[1]->getPrecio());
  							
  							//creamos la orden de compra entre ganador y subastador
  							//
  							if(!$this->ordenDeCompra){
	  							$ordenDeCompra		=		new Default_Model_OrdenDeCompra();
	  							$ordenDeCompra->setComprador($ganador[0]->getUsuario());
	  							$ordenDeCompra->setVendedor($this->usuario);
	  							$ordenDeCompra->setFecha(new DateTime('now'));
	  							$ordenDeCompra->setPrecioFinal($ganador[1]->getPrecio());
	  							$ordenDeCompra->setStatus(1);
	  							$ordenDeCompra->setSubasta($this);
	  							$ordenDeCompra->setVistoComprador(0);
	  							$ordenDeCompra->setVistoComprador(0);
	  							$this->ordenDeCompra	= $ordenDeCompra;
	  							$em->persist($ordenDeCompra);
	  							$em->persist($this);
	  							$em->flush();
  							}
  							
  						}
  					}else if($ganador[0]->getPrecio()>=$this->precioReserva){
  						
  						$this->precioFinal	=	$ganador[0]->getPrecio();
  						$em->persist($this);
  						$em->flush();
  						$data=array("ganador"=>$ganador[0],"precioFinal"=>$ganador[0]->getPrecio());
  						
  						//creamos la orden de compra entre ganador y subastador
  						//
  						if(!$this->ordenDeCompra){
	  						$ordenDeCompra		=		new Default_Model_OrdenDeCompra();
	  						$ordenDeCompra->setComprador($ganador[0]->getUsuario());
	  						$ordenDeCompra->setVendedor($this->usuario);
	  						$ordenDeCompra->setFecha(new DateTime('now'));
	  						$ordenDeCompra->setVistoComprador(0);
	  						$ordenDeCompra->setVistoComprador(0);
	  						$ordenDeCompra->setPrecioFinal($ganador[0]->getPrecio());
	  						$ordenDeCompra->setStatus(1);
	  						$ordenDeCompra->setSubasta($this);
	  						$this->ordenDeCompra	= $ordenDeCompra;
	  						$em->persist($ordenDeCompra);
	  						$em->persist($this);
	  						$em->flush();
  						}
  					}
  				}
  				break;
  			case "holandesa":
  				$query	=	$em->createQuery("	SELECT 	p
													FROM 	Default_Model_Puja p
													WHERE p.articuloSubastado = ?1
  													AND	  p.precio >= ?2
													ORDER BY p.precio DESC, p.fechaPuja ASC
												");
  				$query->setMaxResults(1);
  				$query->setParameter(1,$this);
  				$query->setParameter(2,$this->precioReserva);
  				$ganador				=	$query->getResult();
  				if($ganador){
  					if($ganador[0]->getPrecio()>=$this->precioReserva){
  						$this->precioFinal	=	$ganador[0]->getPrecio();
  						$em->persist($this);
  						$em->flush();
  						$data=array("ganador"=>$ganador[0],"precioFinal"=>$ganador[0]->getPrecio());
  						//creamos la orden de compra entre ganador y subastador
  						//
  						if(!$this->ordenDeCompra){
  						$ordenDeCompra		=		new Default_Model_OrdenDeCompra();
  						$ordenDeCompra->setComprador($ganador[0]->getUsuario());
  						$ordenDeCompra->setVendedor($this->usuario);
  						$ordenDeCompra->setFecha(new DateTime('now'));
  						$ordenDeCompra->setVistoComprador(0);
  						$ordenDeCompra->setVistoComprador(0);
  						$ordenDeCompra->setPrecioFinal($ganador[0]->getPrecio());
  						$ordenDeCompra->setStatus(1);
  						$ordenDeCompra->setSubasta($this);
  						$this->ordenDeCompra	= $ordenDeCompra;
  						$em->persist($ordenDeCompra);
  						$em->persist($this);
  						$em->flush();
  						}
  					}
  						
  				}
  				break;
  				
  		}
  		}
  		return $data;
 	}
  	
  	public function getVista(){						return $this->vistas;		}
  	public function getPrimeraFoto(){	return $this->fotos[0]->getFileName();	}
    
}