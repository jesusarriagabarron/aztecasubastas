<?php 
/**
 * Plugin para comprobar los permisos
 * @author jarriaga
 *
 */
class My_Plugin_Acl extends Zend_Controller_Plugin_Abstract {
  private $_acl = null;
 
  public function __construct(Zend_Acl $acl) {
    $this->_acl = $acl;
  }
 
  public function preDispatch(Zend_Controller_Request_Abstract $request) {
    //As in the earlier example, authed users will have the role user
    $role = (Zend_Auth::getInstance()->hasIdentity())
          ? 'user'
          : 'guest';
 
    //For this example, we will use the controller as the resource:
    $modulo = $request->getModuleName();
    $controller = $request->getControllerName();
    $resource = strtolower($modulo.'-'.$controller);
    $action = $request->getActionName();
    
    if(!$this->_acl->isAllowed($role, $resource) && !$this->_acl->isAllowed($role, $resource,$action)) {
      //If the user has no access we send him elsewhere by changing the request
      $request->setModuleName('default')
              ->setControllerName('error')
              ->setActionName('error9');
    }
  }
}