<?php
/**
 * Clase que administra el manejo de caché para guardar datos usando
 * cache por medio de archivos
 * 
 * @author rlunam
 */
class My_Cache {

	private $cache;

	public function __construct(){
		$this->cache = Zend_Registry::get('cache');
	}
	
	/**
	 * Esta función devuelve el cache solicitado
	 * 
	 */
	public function getData($nombreCache){
		$idCache = $nombreCache.md5($nombreCache);
		$data = $this->cache->load($idCache);
		return $data; 
	}
	
	/**
	 * Esta función crea un nuevo cache de datos
	 * @param unknown $nombreCache
	 * @param unknown $datos
	 * @param number $tiempo
	 */
	public function setData($nombreCache,$datos,$tiempo=1800){
		$idCache = $nombreCache.md5($nombreCache);
		$this->cache->setLifetime($tiempo);
		$this->cache->save($datos,$idCache);
	}
	
	/**
	 * Esta funcion elimina un cache 
	 * @param unknown $nombreCache
	 * @return boolean
	 */
	public function removeData($nombreCache){
		$idCache = $nombreCache.md5($nombreCache);
		$this->cache->remove($idCache);
		return true;
	}
	
	/**
	 * Esta funcion elimina un cache por el id y regenera el cache con los nuevos "datos"
	 * @param unknown $nombreCache
	 * @param unknown $datos
	 * @param number $tiempo
	 */
	public function regenerateData($nombreCache,$datos,$tiempo=1800){
		$idCache = $nombreCache.md5($nombreCache);
		$this->cache->remove($idCache);
		$this->cache->setLifetime($tiempo);
		$this->cache->save($datos,$idCache);
	}
}
?>