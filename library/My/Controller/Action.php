<?php 

/**
 * Definición de clase de controlador genérico
 *
 * @package library.My.Controller
 * @author rlunam
 */

class My_Controller_Action extends Zend_Controller_Action 
{
	/**
	 * Contextos, se pueden modificar en cada uno de los controladores hijos
	 *
	 * @var $contexts: arreglo de contextos
	 */
	public $contexts = array( );
	public $_auth;
	public $_em;
	public $_appid;
	public $_secret;
	
	public function init() {
		date_default_timezone_set('America/Mexico_City');
		
		if($this->_request->isXmlHttpRequest())
			$this->_request->setParam("format","json");
	
		$contextSwitch = $this->_helper->contextSwitch();
		$contextSwitch->initContext();
		
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
		
		$this->getAuth();
	}
	

	private function getAuth() {
		$auth = Zend_Auth::getInstance();
		
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {
			$this->_auth = false;
		}
		
		$this->view->auth 	=	$this->_auth;
	}
	
}