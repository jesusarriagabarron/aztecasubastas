<?php

/**
 * Manejo de envío de correos 
 */
require_once("mandrillSDK/Mandrill.php");

class My_Model_CorreosMandrill	{

	private $_em;
	private $_auth;
	private $_mandrill;
	private $_twitter;
	private $_facebook;

	public function __construct(){
		$registro = Zend_Registry::getInstance();
		$this->_em	=	$registro->entitymanager;
		$auth = Zend_Auth::getInstance();
		if($auth->getStorage()->read())
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		//iniciamos mandrill
		$this->_mandrill = new Mandrill("7a_vHPi_REGrJ4st8aGe7w");
		$this->_facebook	=	"http://www.facebook.com/Endaroo";
		$this->_twitter		=	"http://www.twitter.com/Endaroo";
		
	}
	
	
	public function sendVoucherPago(Default_Model_OrdenDeCompra $odc){
		$template_name= "voucherpago-endaroo";
		$template_content = array(
				array(
						"name" => "example name",
						"content" => "example content"
				)
		);
		$message = array(
				"subject"		=>	"Confirmación de compra en Endaroo",
				"from_email"	=>	"no.reply@endaroo.com",
				"from_name"		=>	"Endaroo",
				"to"			=>	array(
						array(
								"email"		=>		$odc->getComprador()->getEmail(),
								"name"		=>		$odc->getComprador()->getNombreUsuario(),
								"type"		=>		"to"
						)
				),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array("voucher-pago"),
				"global_merge_vars" => array(
						array(
								"nombreUsuario"		 		=> $odc->getVendedor()->getNombreUsuario(),
								"nombreUsuarioCompra"		=> $odc->getComprador()->getNombreUsuario(),
								"CURRENT_YEAR"				=> date("Y"),
								"LIST:COMPANY"				=> "Endaroo"
						)
				),
				"merge_vars" => array(
						array(
								"rcpt" => $odc->getComprador()->getEmail(),
								"vars" => array(
										array(
												"name" => "nombrepub",
												"content" => $odc->getPub()->getTitulo()
										),
										array(
												"name" => "company",
												"content" => "Endaroo"
										),
										array(
												"name" => "pagototal",
												"content" => "$".money_format('%=*(#10.2n', $odc->getPrecioMencion())." MXN" 
										),
										array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
										array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
								)
						)
				),
				"recipient_metadata" => array(
						array(
								"rcpt" => $odc->getComprador()->getEmail(),
								"values" => array("user_id" => $odc->getComprador()->getId())
						)
				),
		);// Fin array message
				
		$async = false;
		$ip_pool = "Main Pool";
		$send_at = date("Y-m-d hh:ii:ss",strtotime("-1 day"));
		try{
			$result = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
		} catch(Mandrill_Error $e) {
		
		}
	}
	
	/**
	 * Envía un mensaje al vendedor, avisandole que tiene una nueva orden de compra
	 * usando Mandrill
	 * @param Default_Model_OrdenDeCompra $odc
	 */
	public function sendNuevaOrden(Default_Model_OrdenDeCompra $odc){
			$template_name= "nuevaordendecompra-endaroo";
			$template_content = array(
					array(
							"name" => "example name",
							"content" => "example content"
					)
			);
			$message = array(
				"subject"		=>	"Felicitaciones! Tienes una orden de compra",
				"from_email"	=>	"no.reply@endaroo.com",
				"from_name"		=>	"Endaroo",				
				"to"			=>	array(
										array(
										"email"		=>		$odc->getVendedor()->getEmail(),
										"name"		=>		$odc->getVendedor()->getNombreUsuario(),
										"type"		=>		"to"
										)
				),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array("compra-nueva"),
				"global_merge_vars" => array(
							array(
									"nombreUsuario"		 		=> $odc->getVendedor()->getNombreUsuario(),
									"nombreUsuarioCompra"		=> $odc->getComprador()->getNombreUsuario(),
									"CURRENT_YEAR"				=> date("Y"),
									"LIST:COMPANY"				=> "Endaroo"
							)
				),
				"merge_vars" => array(
							array(
									"rcpt" => $odc->getVendedor()->getEmail(),
									"vars" => array(
											array(
													"name" => "nombreusuario",
													"content" => $odc->getVendedor()->getNombreUsuario()
											),
											array(
													"name" => "nombreusuariocompra",
													"content" => $odc->getComprador()->getNombreUsuario()
											),
											array(
													"name" => "company",
													"content" => "Endaroo"
											),
											array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
											array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
									)
							)
				),
				"recipient_metadata" => array(
							array(
									"rcpt" => $odc->getVendedor()->getEmail(),
									"values" => array("user_id" => $odc->getVendedor()->getId())
							)
				),
			);// Fin array message
			$async = false;
			$ip_pool = "Main Pool";
			$send_at = date("Y-m-d hh:ii:ss",strtotime("-1 day"));
			try{
				$result = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
			} catch(Mandrill_Error $e) {
				
			}
	}
	
	/**
	 * Envía un correo genérico para un usuario mediante el uso de mandrill,
	 * los datos son los que salen directamente en el correo
	 * @param string $tituloPrincipal
	 * @param string $saludoNombreUsuario
	 * @param string $mensaje1
	 * @param string $mensaje2
	 * @param string $nombrelink1
	 * @param string $link
	 */
	public function sendGenerico(Default_Model_Usuario $usuario,$tituloCorreo,$tituloPrincipal,$saludoNombreUsuario,$mensaje1,$mensaje2,$nombrelink1,$link,$tag){
		$template_name= "generico-endaroo";
		$template_content = array(
				array(
						"name" => "example name",
						"content" => "example content"
				)
		);
		$message = array(
				"subject"		=>	$tituloCorreo,
				"from_email"	=>	"no.reply@endaroo.com",
				"from_name"		=>	"Endaroo",
				"to"			=>	array(
						array(
								"email"		=>		$usuario->getEmail(),
								"name"		=>		$usuario->getNombreUsuario(),
								"type"		=>		"to"
						)
				),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array($tag),
				"global_merge_vars" => array(
						array(
								"CURRENT_YEAR"				=> date("Y"),
								"LIST:COMPANY"				=> "Endaroo"
						)
				),
				"merge_vars" => array(
						array(
								"rcpt" => $usuario->getEmail(),
								"vars" => array(
										array(
												"name" => "nombreusuario",
												"content" => $saludoNombreUsuario
										),
										array(
												"name"		=>	"tituloprincipal",
												"content"	=>	$tituloPrincipal
										),
										array(
												"name"		=>	"mensaje1",
												"content"	=>	$mensaje1
										),
										array(
												"name" => "mensaje2",
												"content" => $mensaje2
										),
										array(
												"name"		=>	"nombrelink1",
												"content"	=>	$nombrelink1
										),
										array(
												"name"		=>	"link1",
												"content"	=>	$link
										),
										array(
												"name" => "company",
												"content" => "Endaroo"
										),
										array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
										array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
								)
						)
				),
				"recipient_metadata" => array(
						array(
								"rcpt" => $usuario->getEmail(),
								"values" => array("user_id" => $usuario->getId())
						)
				),
		);// Fin array message
				$async = false;
				$ip_pool = "Main Pool";
				$send_at = date("Y-m-d hh:ii:ss",strtotime("-1 day"));
				try{
					$result = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
				} catch(Mandrill_Error $e) {
		
				}
		
	}
	
	
	/**
	 * Envia un email notificando que se tiene un mensaje nuevo
	 */
	public function sendNuevoMensaje(Default_Model_Usuario $destino,Default_Model_Usuario $origen,$mensajeEscrito){
		$template_name= "tienesmensaje-endaroo";
		$template_content = array(
				array(
						"name" => "example name",
						"content" => "example content"
				)
		);
		$message = array(
				"subject"		=>	"Tienes un mensaje nuevo de: ".$origen->getNombreUsuario(),
				"from_email"	=>	"no.reply@endaroo.com",
				"from_name"		=>	"Endaroo",
				"to"			=>	array(
						array(
								"email"		=>		$destino->getEmail(),
								"name"		=>		$destino->getNombreUsuario(),
								"type"		=>		"to"
						)
				),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array("tienes-mensaje"),
				"global_merge_vars" => array(
						array(
								"CURRENT_YEAR"				=> date("Y"),
								"LIST:COMPANY"				=> "Endaroo"
						)
				),
				"merge_vars" => array(
						array(
								"rcpt" => $destino->getEmail(),
								"vars" => array(
										array(
												"name" => "nombreusuario",
												"content" => $destino->getNombreUsuario()
										),
										array(
												"name" => "nombreusuariopregunton",
												"content" => $origen->getNombreUsuario()
										),
										array(
												"name" => "company",
												"content" => "Endaroo"
										),
										array(
												"name" => "mensaje",
												"content" => $mensajeEscrito
										),
										array(
												"name" => "misconversaciones",
												"content" => "http://demo.endaroo.com/cuenta/inbox"
										),
										array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
										array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
								)
						)
				),
				"recipient_metadata" => array(
						array(
								"rcpt" => $destino->getEmail(),
								"values" => array("user_id" => $destino->getId())
						)
				),
		);// Fin array message
				$async = false;
				$ip_pool = "Main Pool";
				$send_at = date("Y-m-d hh:ii:ss",strtotime("-1 day"));
				try{
					$result = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
				} catch(Mandrill_Error $e) {
		
				}
	}

}