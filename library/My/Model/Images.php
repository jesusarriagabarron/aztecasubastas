<?php

/**
 * Clase para menejo de archivos [upload, rename, erase]
 * @author rlunam
 *
 */

class My_Model_Images extends Zend_Db_Table_Abstract{
	
	private $_filename;
	private $_path;
	private $_mimetype;
	private $_minsize;
	private $_maxsize;
	
	/**
	 * Sube la imagen de perfil
	 */
	public function uploadfile($idElemento) {
		
		$filename = '';
		$ruta = $_SERVER['DOCUMENT_ROOT'].'/file/perfil/';
	
		$upload = new Zend_File_Transfer_Adapter_Http();
		$upload->setDestination($ruta);
	
		$upload->addValidator('MimeType',  false, 'image');
		$upload->addValidator('FilesSize', false,  array('min' => '10kb', 'max' => '5mb'));
	
		$file = $upload->getFileInfo();
		if($file['user_profile_attributes_photo']['name']!=''){
			
			$extencion = substr(strrchr($file['user_profile_attributes_photo']['name'], '.'), 1);
			$filename  = 'perfil_usuario_'.$idElemento.'_'.date('Y_m_d_H_i_s').'.'.$extencion;
	
			$upload->addFilter('Rename', array(
					'target' => $ruta.'/'.$filename,
					'overwrite' => true
			));
			
			try {
				if (!$upload->receive()) {
					$messages = $upload->getMessages();
					return false;
				} else {
					return $filename;
				}
			} catch (Zend_File_Transfer_Exception $e) {
				$message = $e->getMessage();
				return false;
			}
		} else {
			$filename = '';
			return false;
		}
	}
}