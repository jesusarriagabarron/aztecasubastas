<?php 
/**
 * Modelo para insertar en la base de datos los Articulos subastados
 * @author rlunam
 *
 */
class My_Model_Subasta_ArticuloSubasta extends Zend_Db_Table_Abstract{

	protected $_name = 'Articulosubastado';
		protected $_primary = 'id';
		
		private $_em;
		private $_auth;
		
		public function init(){
			//iniciamos variables globales para manejo de inicio de sesion
			// y manejo del ENTITY MANAGER DOCTRINE
			$auth = Zend_Auth::getInstance();
			if($auth->getStorage()->read())
				$this->_auth = get_object_vars( $auth->getStorage()->read());
		
			$registry = Zend_Registry::getInstance();
			$this->_em = $registry->entitymanager;
		}
	
		/**
		 * Guarda una nueva subasta
		 * @param unknown $request
		 */
	public function nuevaSubasta($request){
		
		//validamos los datos de la solicitud
		$validador = new My_Validador();
		
		$filtro = new Zend_Filter_Alnum(array('allowwhitespace'=>true));
		$inputTitulo = $filtro->filter($request->getParam('inputTitulo'));
			
		$filtro = new Zend_Filter_Int();
		
		$inputCategoria = $filtro->filter($request->getParam('inputCategoria'));
		
		$filtro = new Zend_Filter_Alnum(array('allowwhitespace'=>true));
		$inputDescripcion= $validador->wysiwyg($request->getParam('inputDescripcion'));
		
		$filtro = new Zend_Filter_Alnum(array('allowwhitespace'=>true));
		
		$envio="<strong>Envío:</strong> ". $filtro->filter($request->getParam("envio"));
		$entrega="<strong>Entrega</strong>: ". $filtro->filter( $request->getParam("entrega"));
		$devoluciones="<strong>Devoluciones:</strong> ". $filtro->filter($request->getParam("devoluciones"));
		$pagos="<strong>Pagos</strong>:Visa, Mastercard, Oxxo vía Conekta";
		
		$instrucciones = $envio."||".$entrega."||".$devoluciones."||".$pagos;
		
		$inputInstrucciones= $instrucciones;
		$nombreArchivos 	=	explode(",",$request->getParam('inputNombrearchivo'));
		
		//buscamos la categoria
		$categoria = $this->_em->find("Default_Model_Categoria",$inputCategoria);
		//buscamos el usuario
		$usuario = $this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
	
		//creamos el articulo subastado
		$nuevaSubasta	=	new Default_Model_ArticuloSubastado();
		$nuevaSubasta->setCategoria($categoria);
		$nuevaSubasta->setDescripcion($inputDescripcion);
		$nuevaSubasta->setFechaCreacion();
		$nuevaSubasta->setInstrucciones($inputInstrucciones);
		$nuevaSubasta->setPrecioInicial(0);
		$nuevaSubasta->setPrecioFinal(0);
		$nuevaSubasta->setPrecioReserva(0);
		$nuevaSubasta->setStatus(0);
		$nuevaSubasta->setTitulo($inputTitulo);
		$nuevaSubasta->setUsuario($usuario);

		//guardamos las fotografias
		foreach ($nombreArchivos as $filename){
			$foto	=	new Default_Model_FotosArticulos();
			$foto->setFileName($filename);
			$foto->setArticuloSubastado($nuevaSubasta);
			$this->_em->persist($foto);
		}
		//guardamos las subastas
		$this->_em->persist($nuevaSubasta);
		$this->_em->flush();
		//retornamos el id de la nueva subasta
		return $nuevaSubasta->getId();
	}
	
	
	public function configuraSubasta($request){
		
		//validamos los datos de la solicitud
		$validador 			= new My_Validador();
		$idSubasta			=	$request->getParam("id");
		$idTipoSubasta		=	$request->getParam("inputTipoSubasta");
		$fechaInicio		=	$request->getParam("fechaInicio");
		$horaInicio			=	$request->getParam("horaInicio");
		$fechaCierre		=	$request->getParam("fechaCierre");
		$horaCierre			=	$request->getParam("horaCierre");
		$precioInicial		=	$request->getParam("precioInicial");
		$precioReserva		=	$request->getParam("precioReserva");
		$intevalo			=	$request->getParam("inputIntervalo");
		$items				=	$request->getParam("numeroItems");
		
		$fechahoraInicio	=	new DateTime($fechaInicio." ".$horaInicio);
		$fechahoraFin		=	new DateTime($fechaCierre." ".$horaCierre);

		$TipoSubasta		=	$this->_em->find("Default_Model_TipoSubasta",$idTipoSubasta);
		
		$Subasta			= 	$this->_em->find("Default_Model_ArticuloSubastado",$idSubasta);

		//si es holandesa
		if( $TipoSubasta->getId() == 3){	$Subasta->setIntervalo($intevalo);$Subasta->setPrecioFinal($precioInicial);}
		//si es yankee
		if( $TipoSubasta->getId() == 4 ){	$Subasta->setItems($items);			}
		
		$Subasta->setFechaInicio($fechahoraInicio);
		$Subasta->setFechaCierra($fechahoraFin);
		$Subasta->setPrecioInicial($precioInicial);
		$Subasta->setPrecioReserva($precioReserva);
		$Subasta->setTipoSubasta($TipoSubasta);
		$Subasta->setStatus(1);
		
		$this->_em->persist($Subasta);
		$this->_em->flush();
		
		return true;
		
	}
	
} 



