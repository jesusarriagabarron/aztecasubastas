<?php
/**
 * Devuelve una cantidad en tipo de moneda
 * @author jarriaga
 *
 */

class My_Helper_AlertBox extends Zend_View_Helper_Abstract{

	
	public function alertBox(){
		$messenger = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger');
		$messages =$messenger->getMessages();
		$html = "";
		if(count($messages)){
			foreach ($messages as $message) {
				$explode =  explode("|", $message);
				$tipo = "alert-info";
				$esmensaje = 0;
				switch (trim($explode[0])){
					case "error":
						$esmensaje=1;
						$tipo = "alert-error ";
						$titulo="Error";break;
					case "warning":
						$esmensaje=1;
						$tipo = "";
						$titulo="Advertencia";break;
					case "success":
						$esmensaje=1;
						$tipo = "alert-success ";
						$titulo = "Éxito" ;break;
					case "info":
						$esmensaje=1;
						$tipo = "alert-info ";
						$titulo="Información";break;
				}
			$html.='<div id="messagebox" class="alert '.$tipo.' text-center" style="display: none; font-size:18px!important;
  		margin-left:auto;margin-right:auto;width:940px;">
				  		<button type="button" class="close" data-dismiss="alert">&times;</button>
				  		<strong style="font-size:20px!important;">'.$titulo.'</strong><br>
				  		'.$explode[1].'
				  	</div>';
			}
		}
				
		
		return $html; 		
	}
}