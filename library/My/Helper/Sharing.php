<?php
/**
 * Devuelve la fecha en formato
 * @author plopez
 *
 */

class My_Helper_Sharing extends Zend_View_Helper_Abstract{

	public function Sharing($v=0){
		
		$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		
		if(!$v){
		
		$facebook      = '<div id="shfb" style="position: absolute;"><div class="fb-share-button" data-href="'.$url.'" data-type="button_count"></div></div>';
		$twitterlink   = '<div id="shtw" style="position: absolute; left: 176px; "><a href="'.$url.'" class="twitter-share-button">Tweet</a> </div>';
		$twitterscript = "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
		$googlescript  = '<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>';
		$google        = '<div id="shgg" style="position: absolute;  margin-left: 215px;"><div class="g-plusone"></div></div>';
		 
		$sharing =  $twitterlink.' '.$twitterscript.$facebook.$googlescript.$google;
		}
		else{
			
			$facebook      = '<div id="shfb" ><br><div class="fb-share-button" data-href="'.$url.'" data-type="button"></div></div>';
			$twitterlink   = '<div id="shtw" ><a href="'.$url.'"  class="twitter-share-button" data-size="large" data-count="none">Tweet</a> </div>';
			$twitterscript = "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
			$googlescript  = '<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>';
			$google        = '<div id="shgg" ><br><div class="g-plusone" data-annotation="none"></div></div>';
				
			$sharing =  $twitterlink.' '.$twitterscript.$facebook.$googlescript.$google;
		}
		
		return $sharing;
	}
	
}