<?php
/**
 * Creación de ACL para permisos 
 * @author rlunam
 *
 */
class My_Acl extends Zend_Acl{
	
	public function __construct(){
		//creamos los roles 
		$this->addRole(new Zend_Acl_Role('guest'));
		$this->addRole(new Zend_Acl_Role('user'),'guest');
		$this->addRole(new Zend_Acl_Role('admin'),'user');
		
		//Creamos los recursos
		$this->add(new Zend_Acl_Resource('cuenta-index'));
		$this->add(new Zend_Acl_Resource('cuenta-inbox'));
		$this->add(new Zend_Acl_Resource('cuenta-espaciopublicitario'));
		$this->add(new Zend_Acl_Resource('cuenta-ventas'));
		$this->add(new Zend_Acl_Resource('cuenta-notificaciones'));
		$this->add(new Zend_Acl_Resource('cuenta-compras'));
		$this->add(new Zend_Acl_Resource('cuenta-subasta'));
		$this->add(new Zend_Acl_Resource('default-index'));
		$this->add(new Zend_Acl_Resource('default-mostrarpubs'));
		$this->add(new Zend_Acl_Resource('default-error'));
		$this->add(new Zend_Acl_Resource('users-index'));
		$this->add(new Zend_Acl_Resource('users-registro'));
		$this->add(new Zend_Acl_Resource('users-perfil'));
		$this->add(new Zend_Acl_Resource('users-perfil-publico'));
		$this->add(new Zend_Acl_Resource('schedule-index'));
		
		
		$this->add(new Zend_Acl_Resource('subastas-subasta'));
		$this->add(new Zend_Acl_Resource('subastas-puja'));

		//Creamos los permisos
		//permisos invitado
		$this->allow('guest','default-index');
		$this->allow('guest','default-error');
		$this->allow('guest','users-registro');
		$this->allow('guest','users-perfil','publico');
		
		$this->allow('guest','subastas-subasta');
		$this->allow('user', 'subastas-puja');
		
		//permisos de usuario
		$this->allow('user','cuenta-index');
		$this->allow('user','cuenta-subasta');
		
		$this->allow('user','users-perfil');
		$this->allow('user','cuenta-espaciopublicitario');
		$this->allow('user','cuenta-ventas');
		$this->allow('user','cuenta-compras');
		$this->allow('user','cuenta-inbox');
		$this->allow('user','cuenta-notificaciones');
		$this->allow('user','schedule-index');
	}
	
}