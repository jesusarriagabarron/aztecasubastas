<?php
define("TXT_CONTENIDO", " ,.,-,[,],!,¡,_,?,¿,',:,/,á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,@,(,),<,>,&,:,;,=,\",comma,;,…,’,‘,%,#,“,”,–,$,|,—,+,{,},*,	,°,´?,\\");
define("TXT_WYSIWYG", " ,.,-,[,],!,¡,_,?,¿,',:,/,á,é,í,ó,ú,ñ,Á,É,Í,Ó,Ú,Ñ,@,(,),<,>,&,:,;,=,\",comma,;,…,’,‘,%,#,“,”,–,$,|,—,+,{,},*,	,°,´,\\");

class My_Validador extends Zend_Validate{
	
	public function isValid($programa){
        return $this->urlValida($programa);
    }
	 
	public function urlValida($programa){
	 	
    	$valido = $programa;
    	$filtroST = new Zend_Filter_StripTags();
		$filtroTrim = new Zend_Filter_StringTrim();
		
		$programa =  $filtroST -> filter($programa);
		$programa =  $filtroTrim -> filter($programa);
    	//se sustituye los - que tiene la url para verificar que sea alphanumerco
    	$programa = str_replace("-","",$programa);  	
    	if (!Zend_Validate::is($programa, 'Alnum')) {
			$valido = false;
		}
		
		return $valido;
    }

    /**
     * Funcion que valida los enteros
     * @param int $value
     * @return int (validado, filtrado y escapdo) 
     */
    public function intValido($value){
    	// valida entero
    	if(Zend_Validate::is($value, 'Int')){
    		// filtrarlo
    		$Zfilter = new Zend_Filter_Int();
    		$value = $Zfilter->filter($value, 'Int');
    		return $value;
    	}
    	return false;
    }
    
	/**
     * Funcion que valida las cadenas de solo caracteres
     * @param int $value
     * @return int (validado, filtrado y escapdo) 
     */
    public function alphaValido($value, $excepciones = null){
        $val = $value;
        if (is_array($excepciones)) {
            foreach ($excepciones as $excepcion => $exc)
                $val = str_replace($exc,"",$val);
        }
    	// valida cadena
    	if(Zend_Validate::is($val, 'Alpha')){
    		// filtrarlo
    		$filtroST = new Zend_Filter_StripTags();
			$filtroTrim = new Zend_Filter_StringTrim();
			
			$value =  $filtroST -> filter($value);
			$value =  $filtroTrim -> filter($value);
    		
    		return $value;
    	}
    	return false;
    }
    

	/**
     * Funcion que valida las cadenas de caracteres y numeros
     * @param int $value
     * @return int (validado, filtrado y escapdo) 
     */
    public function alphanumValido($value, $excepciones = null){
    	$val = $value;
    	if(!is_array($excepciones) && $excepciones!=null){
    		$excepciones = explode(",", $excepciones);
    	}
    	
        if (is_array($excepciones)) {
            foreach ($excepciones as $excepcion => $exc){
            	if(strcmp("comma",$exc)==0)
            		$val = str_replace(",","",$val);
            	else
                	$val = str_replace($exc,"",$val);
            }
            
        }
        
    	// valida cadena
    	if(Zend_Validate::is($val, 'Alnum')){
    		// filtrarlo
    		$filtroST = new Zend_Filter_StripTags();
			$filtroTrim = new Zend_Filter_StringTrim();
			
			$value =  $filtroST -> filter($value);
			$value =  $filtroTrim -> filter($value);
    		
    		return $value;
    	}
    	return false;
    }
    

	/**
     * Funcion que valida los tipo float
     * @param int $value
     * @return int (validado, filtrado y escapdo) 
     */
    public function floatValido($value){
    	// valida cadena
    	if(Zend_Validate::is($value, 'Float')){
    		// filtrarlo
    		$filtroST = new Zend_Filter_StripTags();
			$filtroTrim = new Zend_Filter_StringTrim();
			
			$value =  $filtroST -> filter($value);
			$value =  $filtroTrim -> filter($value);
    		
    		return $value;
    	}
    	return false;
    }
    
    

	/**
     * Funcion que valida los mails
     * @param int $value
     * @return int (validado, filtrado y escapdo) 
     */
    public function mailValido($value){
    	// valida cadena
    	if(Zend_Validate::is($value, 'EmailAddress')){
    		// filtrarlo
    		$filtroST = new Zend_Filter_StripTags();
			$filtroTrim = new Zend_Filter_StringTrim();
			
			$value =  $filtroST -> filter($value);
			$value =  $filtroTrim -> filter($value);
    		
    		return $value;
    	}
    	return false;
    }
    
    
 	/**
     * Metodo que aplica el filtro StripTags
     * @param string $string
     */
     
    private function stringTags($string) {
    	$filter  = new Zend_Filter_StripTags();
    	return $filter->filter($string);
    }
    
    
    /**
     * Metodo que recibe un elemento, valida si es un objeto, arreglo o variable y aplica validaciones 
     * @param $object
     */
     
    
	public function dataFront($object) {
        if (is_object ( $object ))
		    $object = get_object_vars ($object);
		    
		    
		if (is_array ( $object )){
		    foreach ( $object as $key => $value ) {
		    	//Exception para widgets y contenido de la nota
		    	if($key != 'widgets' && $key != 'cContenido' && $key != 'animacionFlash')
		        	$object [$key] =  $this->dataFront ($object [$key]);
		    }
		} else {
			$object = addslashes($this->stringTags($object));
		}
		
		return $object;
    }
    
    public function arrayValido($data, $tipo) {
        $filtroST = new Zend_Filter_StripTags();
		$filtroTrim = new Zend_Filter_StringTrim();
		if (is_array($data)) {
		    foreach ($data as $value => $val) {
		        $valor = $val;
		        //Aplicamos los filtros
		        $valor =  $filtroST  -> filter($valor);
		        $valor =  $filtroTrim-> filter($valor);
		        //Quitamos los caracteres permitidos
		        $valor = str_replace("-","",$valor);
		        $valor = str_replace(":","",$valor);
		        $valor = str_replace(";","",$valor);
		        //Validamos el tipo dependiendo el tipo que hayamos recibido como parametro
		        if (!Zend_Validate::is($valor, "{$tipo}")) {
			        return false;
		        }
		    }
		    return $data;
		}
        return false;
    }
    
	/**
     * Metodo que recibe una cadena y valida si es un directorio validao, si no lo esta regresa falso 
     * @param $object
     */
     
    
	public function validPath($path) {
		if (file_exists($path)){
        	return $path;
        }
        else{
        	return false;
        }
    }

    /**
     * 
     * Funcion que valida una fecha, recibe como parametro obligatorio la fecha y como parametro opcional el formato de la siguiente manera array('format'=>' formato ')
     * @param unknown_type $fecha
     * @param unknown_type $format
     */
    public function validDate($fecha, $format = '') {
        $validator = new Zend_Validate_Date($format);
		if($validator->isValid($fecha)) {
		    // filtrarlo
    		$filtroST   = new Zend_Filter_StripTags();
			$filtroTrim = new Zend_Filter_StringTrim();
			$fecha =  $filtroST -> filter($fecha);
			$fecha =  $filtroTrim -> filter($fecha);
			//Retornar valor si fue un fecha valida
    		return $fecha;
		}
		return false; //Retornamos un false si fue una fecha no válida
    }
    
    public function validIdKaltura($idKaltura)
    {
    	$valido    = $idKaltura;
    	$idKaltura = str_replace("_","",$idKaltura);
        if (Zend_Validate::is($idKaltura, 'Alnum')) {
		    $filtroST = new Zend_Filter_StripTags();
		    $filtroTrim = new Zend_Filter_StringTrim();
	        //Aplicamos los filtros	
		    $valido = $filtroST -> filter($valido);
		    $valido = $filtroTrim -> filter($valido);
            //Retornamos el valor válido
		    return $valido;	
		}
    	return false;
    }
    
    public function execValida($comando) {
        $valido = false;
        //lista de comandos validos
        $comandosValidos[0]='ffmpeg';
        $comandosValidos[1]='mv';
        $comandosValidos[1]='rm';
        //Validamos si el comando mandado en valido
        foreach ($comandosValidos as $value) {
            if($value == strtolower(substr($comando, 0, strlen($value)))){
                $valido = $comando;
                break;
            }
        }
        return $valido;
    }
    
    public function validFileExtension($fileName) {
        $arrayExt = array('jpeg', 'jpg', 'gif', 'png');
        $value    = explode('.', $fileName);
        if (isset($value[1])) 
            return (in_array($value[1], $arrayExt)) ? $fileName : false;
        else return false;
    }
	

	public function validaSoloAlpha($data)
    {
    	$patron = "/^[a-zA-Z0-9_áéíóúÁÉÍÓÚÑñ ]*$/";
		if (preg_match($patron, $data)) {
			return $data;
		}
		else return false;
    }
    
	public function validarSoloFecha($fecha)
	{
        $reg = "#^((19|20)?[0-9]{2}[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]))*$#";
        if (preg_match($reg, $fecha)) {
			return $fecha;
		} else {
			return false;
		}
    }
    
	/**
     * Metodo que recibe un elemento, valida si es un objeto, arreglo o variable y valida que solo sean enteros cada uno de esos elementos 
     * @param $object
     * [EPG] 2012-04-11
     */
     
    
	public function arrayInts($object) {
        if (is_object ( $object )){
		    $object = get_object_vars ($object);
		   
        }
		    
		    
		if (is_array ( $object )){
		    foreach ( $object as $key => $value ) {
		        	$object [$key] =  $this->arrayInts ($object [$key]);
		    }
		} else {
			$object = $this->intValido($object);
		}
		
		return $object;
    }
    
	/**
     * Metodo que recibe un elemento, valida si es un objeto, arreglo o variable y valida que solo sean enteros cada uno de esos elementos 
     * @param $object
     * [EPG] 2012-05-14
     */
     
    
	public function arrayAlphanum($object,$excepciones) {
        if (is_object ( $object )){
		    $object = get_object_vars ($object);
		   
        }
		    
		    
		if (is_array ( $object )){
		    foreach ( $object as $key => $value ) {
		        	$object [$key] =  $this->arrayAlphanum($object [$key],$excepciones);
		    }
		} else {
			$object = $this->alphanumValido($object,$excepciones);
		}
		
		return $object;
    }
    
    
	/**
     * Metodo que recibe una cadena de fecha y hora la valida para los siguientes formatos yyyy-mm-dd hh:mm o yyyy-mm-dd hh:mm:ss 
     * [EPG] 2012-04-13
     */    
    
	public function fechaHora($fecha){
		if (preg_match("/(19|20)[0-9]{2}[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[ ]([01][0-9]|2[0-3])[:]([0-5][0-9])([:][0-5][0-9])?$/", $fecha))
			return $fecha;
		else 
			return false;
	}
	
	/**
	 * 
	 * Funcion que valida si una url es válida, devuelve falso si no, si es válida entonces regresa la url
	 * @param string $url
	 * @return bool | string
	 */
	public function validUrl($url) { return (filter_var($url,FILTER_VALIDATE_URL,FILTER_FLAG_QUERY_REQUIRED) === false) ? false : $url; }
	

	/**
     * Funcion que valida la cadena de caracteres generada por wysiwyg
     */
    public function wysiwyg($value){
    	str_replace(" ", " ", $value);
    	
        $val = $value;
        
        $datos = bin2hex($val);
        for($i = 0 ; $i < strlen($datos) ; $i+=2 )
        	$chars[] = substr($datos,$i,2);
        
        $val = "";
        
        foreach($chars as $c)
        	if($c!="0a")
        		$val = $val.chr(hexdec($c));
        
		$excepciones = explode(",", TXT_WYSIWYG);
    		
        if (is_array($excepciones)) {
            foreach ($excepciones as $excepcion => $exc){
            	if(strcmp("comma",$exc)==0)
            		$val = str_replace(",","",$val);
            	else
                	$val = str_replace($exc,"",$val);
            }
        }
        
    	// valida cadena
    	if(Zend_Validate::is($val, 'Alnum')){
    		// filtrarlo
			$filtroTrim = new Zend_Filter_StringTrim();
			
			$value =  $filtroTrim -> filter($value);
   
    		return $value;
    	}
    	else{
			return false;	
    	}
    	
    }
    
    /**
     * 
     * Funcion que validara que la ruta que se ha ingresado sea valida y exista
     * @param unknown_type $url
     */
    public function pathFileValid($url) {
        $value = (string) $url;
        //Declaramos y aplicamos los filtros a la cadena temporal
    	$filtroST   = new Zend_Filter_StripTags();
		$filtroTrim = new Zend_Filter_StringTrim();
		$value =  $filtroST->filter($value);
		$value =  $filtroTrim->filter($value);
        
        if (!Zend_Uri::check($value)) { return false; }
        else {
            if (file_exists($url)) return $url;
            else return false;
        }
        return true;
    }
    
    /**
     * Función que validará la cadena enviada para determinar si es el codigo hexadecimal de un color
     * @param string $colorCode
     * @return string | boolean
     */
    public function isHex($colorCode) {
    	if (preg_match('/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/', $colorCode)) return $colorCode;
    	else false;
    }
    
    /**
     * En caso de que el path por validad aun no existe y apenas se va crear.
     * Si el path enviado tiene caracteres raros u otros comandos regresa false.
     *
     * @param string $path
     */
    public function pathValidoMkdir($path){
    	 
    	$buscar = array('cp', ';', 'mv', 'ls', 'll', '-', '|');
    	foreach ($buscar as $item){
    		$resultado = strpos($path, $item);
    		if($resultado === true){
    			return false;
    		}
    	}
    	return $path;
    }
    public function validDomain($domain) {
    	$domains = array("www.tvazteca.com","www.aztecanoticias.com.mx","www.aztecadeportes.com","www.aztecaespectaculos.com",
    				     "cms.tvazteca.com","cms.tvazteca.local","cms.tvazteca.desa","www.azteca.com");
    	if (in_array($domain,$domains)){
    		return $domain;
    	}
    	else{
    		return false;
    	}
    }
    
    /**
     * Valida que el parametro contenga un string de HTML que comienza
     * y Temrmina como Tabla
     *
     * @param string $string
     */
    public function validHTMLTable($string){
    
    	$tableMatchStart = preg_match('/<table(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>/', $string );
    	
    	$tableMatchEnd = preg_match('/<\/table>/', $string );
    	
    	if($tableMatchStart != 0){
    		if($tableMatchEnd != 0){
    		 return $string;
    		}
    	}else{
    		return false;
    	}
    	
    }
    /**
     * Metodo que recibe un elemento, valida si es un objeto, arreglo o variable y valida que solo sean enteros cada uno de esos elementos
     * @param $object
     * [EPG] 2012-05-14
     */
     
    public function arraywyswyg($object) {
    	if (is_object ( $object ))
    		$object = get_object_vars ($object);
    		    
    	if (is_array ( $object ))
    		foreach ( $object as $key => $value ) 
    			$object [$key] =  $this->arraywyswyg($object [$key]);
    	else 
    		$object = $this->wysiwyg($object);
    
    	return $object;
    }    
}