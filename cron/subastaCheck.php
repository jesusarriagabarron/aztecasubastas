<?php 
defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));


class subastaCheck {
	
	private $_mysql;
	private $_desarrollo=true;
	
	
	public function conectarbase(){
		if($this->_desarrollo)
			$this->_mysql	=	new mysqli("127.0.0.1", "root", "root", "aztecasubastas");
		else
			$this->_mysql	=	new mysqli("aztecaSubs10.db.12040299.hostedresource.com", "aztecaSubs10", "foto10@PRO55","aztecaSubs10");

	}	
	
	
	public function __construct(){
		$this->conectarbase();
		if($this->_mysql->connect_errno){
				throw new Exception("Error al conectar la base ".$this->_mysql->error);
		}
	}
	/**
	 * Devuelve todos las subastas
	 */
	public function getSubastasActivas(){
		$date = new DateTime('now');
		$result = $date->format('Y-m-d H:i:s');
		$sql	=	"	SELECT  		id, fechaCierre
						FROM 			Articulosubastado
						WHERE			fechaCierre<='".$result."'
						AND				status=1";
	
		$subastas		=		$this->_mysql->query($sql);
	
		return $subastas;
	}

	/** verifica el status de las subastas
	 * 
	 */
	public function verificaVencidas(){

		$subastas 	= 	$this->getSubastasActivas();
		$subastas->data_seek(0);
		//recorremos las subastas	
		while($subasta	=	$subastas->fetch_assoc()){
				//cambiamos el status de la subasta a finalizada
				$sql	=	"	UPDATE 	Articulosubastado
								SET		status=2
								WHERE	id={$subasta["id"]}
								LIMIT	1";
				$this->_mysql->query( $sql );
		}//fin  FOREACH

	}
	
}

