<?php 
defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));

require_once(CRON_PATH."/../library/conektaSDK/Conekta.php");

class oxxoCheck {
	
	private $_mysql;
	private $_desarrollo=false;
	
	
	public function conectarbase(){
		if($this->_desarrollo)
			$this->_mysql	=	new mysqli("127.0.0.1", "root", "root", "aztecasubastas");
		else
			$this->_mysql	=	new mysqli("aztecaSubs10.db.12040299.hostedresource.com", "aztecaSubs10", "foto10@PRO55", "aztecaSubs10");
	}	
	
	
	public function __construct(){
		$this->conectarbase();
		Conekta::setApiKey("key_1Zd4q1qWPdUmMP1Z");
		if($this->_mysql->connect_errno){
				throw new Exception("Error al conectar la base ".$this->_mysql->error);
		}
	}
	
	/**
	 * Devuelve todos los pagos de Oxxo donde las boletas estan inactivas
	 */
	public function getPagosOxxo24horas(){
		$sql	=	"	SELECT  		B.id,B.comprador_id,B.vendedor_id,B.status,B.subastaId,
										C.status as statusCargo,B.chargeId,
										C.payment_method,C.expires_at
						FROM 			OrdenDeCompra B inner join Cargo C 
						ON 				B.chargeId = C.chargeId
						WHERE			C.payment_method = 'oxxo'
						AND				B.status=1";
		
		$pagosOxxo		=		$this->_mysql->query($sql);
		return $pagosOxxo;		
	}
	
	/** verifica el status de los pagos en Conekta 
	 * 
	 */
	public function verificaPagos(){
		$pagos 	= 	$this->getPagosOxxo24horas();
		$pagos->data_seek(0);
		//recorremos los pagos
		while($pago	=	$pagos->fetch_assoc()){
			//regresamos el cargo de conecta
			$charge = Conekta_Charge::find($pago["chargeId"]);
			//guardamos las variables
			$chargeId		=	$charge->id;
			$idSubasta		=	$pago["subastaId"];
			$idODC			=	$pago["id"];
			$statusCargo	=	$charge->status;
			//si el cargo esta pagado
			if($statusCargo="paid"){
				$sql	=	"	UPDATE 	OrdenDeCompra
								SET		status=2,vistoComprador=0,vistoVendedor=0
								WHERE	id={$idODC}
								LIMIT	1";
				$this->_mysql->query( $sql );

			}//fin   IF
		}//fin  FOREACH
	}
	
}

$oxxo 	= new oxxoCheck();
$oxxo->verificaPagos();
